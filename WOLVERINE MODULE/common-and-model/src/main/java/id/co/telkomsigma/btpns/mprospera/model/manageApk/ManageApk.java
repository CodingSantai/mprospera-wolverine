package id.co.telkomsigma.btpns.mprospera.model.manageApk;

import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.springframework.web.multipart.MultipartFile;
import id.co.telkomsigma.btpns.mprospera.model.GenericModel;
import id.co.telkomsigma.btpns.mprospera.model.user.User;

@Entity
@Table(name = "M_MANAGE_APK")
public class ManageApk extends GenericModel {

    /**
     *
     */
    private static final long serialVersionUID = 7774577867144408027L;

    private Long apkId;
    private String apkVersion;
    private String changeDescription;
    private byte[] apkFile;
    private User createdBy;
    private Date createdDate;
    private User updateBy;
    private Date updateDate;
    private boolean allowedVersion;
    // transient value
    private String name;
    private MultipartFile file;

    @Id
    @Column(name = "id", nullable = false, unique = true)
    @GeneratedValue
    public Long getApkId() {
        return apkId;
    }

    public void setApkId(Long apkId) {
        this.apkId = apkId;
    }

    @Column(name = "apk_version", nullable = false, length = 50)
    public String getApkVersion() {
        return apkVersion;
    }

    public void setApkVersion(String apkVersion) {
        this.apkVersion = apkVersion;
    }

    @Column(name = "change_description", nullable = true, length = 255)
    public String getChangeDescription() {
        return changeDescription;
    }

    public void setChangeDescription(String changeDescription) {
        this.changeDescription = changeDescription;
    }

    @Column(name = "apk_file", nullable = false)
    public byte[] getApkFile() {
        return apkFile;
    }

    public void setApkFile(byte[] apkFile) {
        this.apkFile = apkFile;
    }

    @ManyToOne(targetEntity = User.class, fetch = FetchType.LAZY)
    @JoinColumn(name = "CREATED_BY", nullable = false)
    public User getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(User createdBy) {
        this.createdBy = createdBy;
    }

    @Column(name = "CREATED_DT", nullable = false)
    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    @ManyToOne(targetEntity = User.class)
    @JoinColumn(name = "UPDATE_BY", nullable = false)
    public User getUpdateBy() {
        return updateBy;
    }

    public void setUpdateBy(User updateBy) {
        this.updateBy = updateBy;
    }

    @Column(name = "UPDATE_DT", nullable = false)
    public Date getUpdateDate() {
        return updateDate;
    }

    public void setUpdateDate(Date updateDate) {
        this.updateDate = updateDate;
    }

    @Transient
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Transient
    public MultipartFile getFile() {
        return file;
    }

    public void setFile(MultipartFile file) {
        this.file = file;
    }

    @Column(name = "IS_ALLOWED", nullable = false)
    public boolean isAllowedVersion() {
        return allowedVersion;
    }

    public void setAllowedVersion(boolean allowedVersion) {
        this.allowedVersion = allowedVersion;
    }

}