package id.co.telkomsigma.btpns.mprospera.response;

import java.util.List;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import id.co.telkomsigma.btpns.mprospera.request.*;

@SuppressWarnings("ALL")
@JsonSerialize(include = JsonSerialize.Inclusion.NON_EMPTY)
public class ListSWResponse {

    private String swId;
    private String createdAt;
    private String createdBy;
    private String updatedAt;
    private String sentraId;
    private String sentraName;
    private String groupId;
    private String groupName;
    private String customerCif;
    private String customerId;
    private String customerName;
    private String customerType;
    private String dayLight;
    private String idCardName;
    private String idCardNumber;
    private String pmId;
    private String rejectedReason;
    private String surveyDate;
    private String disbursementDate;
    private String swLocation;
    private String status;
    private String hasIdPhoto;
    private String hasBusinessPlacePhoto;
    private String hasApprovedMs;
    private String customerExists;
    private String loanExists;
    private String isNasabahPotensial;
    private WismaSWReq wisma;
    private SwAddressResponse address;
    private SWProfileResponse profile;
    private SWExpenseResponse expense;
    private ItemCalcLainReq itemCalcLain;
    private SWPartnerResponse partner;
    private SWBusinessResponse business;
    private SWCalculationVariable calcVariable;
    private List<AWGMBuyList> awgmPurchasing;
    private List<DirectBuyList> directPurchasing;
    private List<ReferenceList> referenceList;
    private List<SWProductListResponse> swProducts;
    private List<SWApprovalHistory> approvalHistories;

    public String getSwId() {
        return swId;
    }

    public void setSwId(String swId) {
        this.swId = swId;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

    public String getSentraId() {
        return sentraId;
    }

    public void setSentraId(String sentraId) {
        this.sentraId = sentraId;
    }

    public String getSentraName() {
        return sentraName;
    }

    public void setSentraName(String sentraName) {
        this.sentraName = sentraName;
    }

    public String getGroupId() {
        return groupId;
    }

    public void setGroupId(String groupId) {
        this.groupId = groupId;
    }

    public String getGroupName() {
        return groupName;
    }

    public void setGroupName(String groupName) {
        this.groupName = groupName;
    }

    public String getCustomerCif() {
        return customerCif;
    }

    public void setCustomerCif(String customerCif) {
        this.customerCif = customerCif;
    }

    public String getCustomerId() {
        return customerId;
    }

    public void setCustomerId(String customerId) {
        this.customerId = customerId;
    }

    public String getCustomerName() {
        return customerName;
    }

    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }

    public String getCustomerType() {
        return customerType;
    }

    public void setCustomerType(String customerType) {
        this.customerType = customerType;
    }

    public String getDayLight() {
        return dayLight;
    }

    public void setDayLight(String dayLight) {
        this.dayLight = dayLight;
    }

    public String getIdCardName() {
        return idCardName;
    }

    public void setIdCardName(String idCardName) {
        this.idCardName = idCardName;
    }

    public String getIdCardNumber() {
        return idCardNumber;
    }

    public void setIdCardNumber(String idCardNumber) {
        this.idCardNumber = idCardNumber;
    }

    public String getPmId() {
        return pmId;
    }

    public void setPmId(String pmId) {
        this.pmId = pmId;
    }

    public String getRejectedReason() {
        return rejectedReason;
    }

    public void setRejectedReason(String rejectedReason) {
        this.rejectedReason = rejectedReason;
    }

    public String getSurveyDate() {
        return surveyDate;
    }

    public void setSurveyDate(String surveyDate) {
        this.surveyDate = surveyDate;
    }

    public String getSwLocation() {
        return swLocation;
    }

    public void setSwLocation(String swLocation) {
        this.swLocation = swLocation;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getHasIdPhoto() {
        return hasIdPhoto;
    }

    public void setHasIdPhoto(String hasIdPhoto) {
        this.hasIdPhoto = hasIdPhoto;
    }

    public String getHasBusinessPlacePhoto() {
        return hasBusinessPlacePhoto;
    }

    public void setHasBusinessPlacePhoto(String hasBusinessPlacePhoto) {
        this.hasBusinessPlacePhoto = hasBusinessPlacePhoto;
    }

    public String getHasApprovedMs() {
        return hasApprovedMs;
    }

    public void setHasApprovedMs(String hasApprovedMs) {
        this.hasApprovedMs = hasApprovedMs;
    }

    public String getCustomerExists() {
        return customerExists;
    }

    public void setCustomerExists(String customerExists) {
        this.customerExists = customerExists;
    }

    public String getLoanExists() {
        return loanExists;
    }

    public void setLoanExists(String loanExists) {
        this.loanExists = loanExists;
    }

    public WismaSWReq getWisma() {
        return wisma;
    }

    public void setWisma(WismaSWReq wisma) {
        this.wisma = wisma;
    }

    public SwAddressResponse getAddress() {
        return address;
    }

    public void setAddress(SwAddressResponse address) {
        this.address = address;
    }

    public SWProfileResponse getProfile() {
        return profile;
    }

    public void setProfile(SWProfileResponse profile) {
        this.profile = profile;
    }

    public SWExpenseResponse getExpense() {
        return expense;
    }

    public void setExpense(SWExpenseResponse expense) {
        this.expense = expense;
    }

    public ItemCalcLainReq getItemCalcLain() {
        return itemCalcLain;
    }

    public void setItemCalcLain(ItemCalcLainReq itemCalcLain) {
        this.itemCalcLain = itemCalcLain;
    }

    public SWPartnerResponse getPartner() {
        return partner;
    }

    public void setPartner(SWPartnerResponse partner) {
        this.partner = partner;
    }

    public SWBusinessResponse getBusiness() {
        return business;
    }

    public void setBusiness(SWBusinessResponse business) {
        this.business = business;
    }

    public SWCalculationVariable getCalcVariable() {
        return calcVariable;
    }

    public void setCalcVariable(SWCalculationVariable calcVariable) {
        this.calcVariable = calcVariable;
    }

    public List<AWGMBuyList> getAwgmPurchasing() {
        return awgmPurchasing;
    }

    public void setAwgmPurchasing(List<AWGMBuyList> awgmPurchasing) {
        this.awgmPurchasing = awgmPurchasing;
    }

    public List<DirectBuyList> getDirectPurchasing() {
        return directPurchasing;
    }

    public void setDirectPurchasing(List<DirectBuyList> directPurchasing) {
        this.directPurchasing = directPurchasing;
    }

    public List<ReferenceList> getReferenceList() {
        return referenceList;
    }

    public void setReferenceList(List<ReferenceList> referenceList) {
        this.referenceList = referenceList;
    }

    public List<SWProductListResponse> getSwProducts() {
        return swProducts;
    }

    public void setSwProducts(List<SWProductListResponse> swProducts) {
        this.swProducts = swProducts;
    }

    public List<SWApprovalHistory> getApprovalHistories() {
        return approvalHistories;
    }

    public void setApprovalHistories(List<SWApprovalHistory> approvalHistories) {
        this.approvalHistories = approvalHistories;
    }

    public String getDisbursementDate() {
        return disbursementDate;
    }

    public void setDisbursementDate(String disbursementDate) {
        this.disbursementDate = disbursementDate;
    }

    public String getIsNasabahPotensial() {
        return isNasabahPotensial;
    }

    public void setIsNasabahPotensial(String isNasabahPotensial) {
        this.isNasabahPotensial = isNasabahPotensial;
    }

    @Override
    public String toString() {
        return "ListSWResponse{" +
                "swId='" + swId + '\'' +
                ", createdAt='" + createdAt + '\'' +
                ", createdBy='" + createdBy + '\'' +
                ", updatedAt='" + updatedAt + '\'' +
                ", sentraId='" + sentraId + '\'' +
                ", sentraName='" + sentraName + '\'' +
                ", groupId='" + groupId + '\'' +
                ", groupName='" + groupName + '\'' +
                ", customerCif='" + customerCif + '\'' +
                ", customerId='" + customerId + '\'' +
                ", customerName='" + customerName + '\'' +
                ", customerType='" + customerType + '\'' +
                ", dayLight='" + dayLight + '\'' +
                ", idCardName='" + idCardName + '\'' +
                ", idCardNumber='" + idCardNumber + '\'' +
                ", pmId='" + pmId + '\'' +
                ", rejectedReason='" + rejectedReason + '\'' +
                ", surveyDate='" + surveyDate + '\'' +
                ", disbursementDate='" + disbursementDate + '\'' +
                ", swLocation='" + swLocation + '\'' +
                ", status='" + status + '\'' +
                ", hasIdPhoto='" + hasIdPhoto + '\'' +
                ", hasBusinessPlacePhoto='" + hasBusinessPlacePhoto + '\'' +
                ", hasApprovedMs='" + hasApprovedMs + '\'' +
                ", customerExists='" + customerExists + '\'' +
                ", loanExists='" + loanExists + '\'' +
                ", isNasabahPotensial='" + isNasabahPotensial + '\'' +
                ", wisma=" + wisma +
                ", address=" + address +
                ", profile=" + profile +
                ", expense=" + expense +
                ", itemCalcLain=" + itemCalcLain +
                ", partner=" + partner +
                ", business=" + business +
                ", calcVariable=" + calcVariable +
                ", awgmPurchasing=" + awgmPurchasing +
                ", directPurchasing=" + directPurchasing +
                ", referenceList=" + referenceList +
                ", swProducts=" + swProducts +
                ", approvalHistories=" + approvalHistories +
                '}';
    }

}