package id.co.telkomsigma.btpns.mprospera.response;

import java.util.Date;
import java.util.List;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;

@SuppressWarnings("ALL")
@JsonSerialize(include = JsonSerialize.Inclusion.NON_EMPTY)
public class ListPMResponse {

    private String pmId;
    private String areaName;
    private String areaId;
    private String createdBy;
    private String createdDate;
    private String pmParticipant;
    private String swCandidate;
    private String pmLocationName;
    private String pmOwner;
    private String status;
    private String time;
    private String pmDate;
    private String pmPlanDate;
    private String pmOwnerPhoneNumber;
    private String mmId;
    private String localId;
    private String updatedDate;
    private String updatedBy;   
    private List<ListSWResponse> swList;

    public String getPmOwner() {
        return pmOwner;
    }

    public void setPmOwner(String pmOwner) {
        this.pmOwner = pmOwner;
    }

    public String getPmId() {
        return pmId;
    }

    public void setPmId(String pmId) {
        this.pmId = pmId;
    }

    public String getAreaName() {
        return areaName;
    }

    public void setAreaName(String areaName) {
        this.areaName = areaName;
    }

    public String getAreaId() {
        return areaId;
    }

    public void setAreaId(String areaId) {
        this.areaId = areaId;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public String getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(String createdDate) {
        this.createdDate = createdDate;
    }

    public String getPmLocationName() {
        return pmLocationName;
    }

    public void setPmLocationName(String pmLocationName) {
        this.pmLocationName = pmLocationName;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getSwCandidate() {
        return swCandidate;
    }

    public void setSwCandidate(String swCandidate) {
        this.swCandidate = swCandidate;
    }

    public String getPmDate() {
        return pmDate;
    }

    public void setPmDate(String pmDate) {
        this.pmDate = pmDate;
    }

    public List<ListSWResponse> getSwList() {
        return swList;
    }

    public void setSwList(List<ListSWResponse> swList) {
        this.swList = swList;
    }

    public String getPmOwnerPhoneNumber() {
        return pmOwnerPhoneNumber;
    }

    public void setPmOwnerPhoneNumber(String pmOwnerPhoneNumber) {
        this.pmOwnerPhoneNumber = pmOwnerPhoneNumber;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getPmParticipant() {
        return pmParticipant;
    }

    public void setPmParticipant(String pmParticipant) {
        this.pmParticipant = pmParticipant;
    }

    public String getMmId() {
        return mmId;
    }

    public void setMmId(String mmId) {
        this.mmId = mmId;
    }

    public String getPmPlanDate() {
        return pmPlanDate;
    }

    public void setPmPlanDate(String pmPlanDate) {
        this.pmPlanDate = pmPlanDate;
    }

    public String getLocalId() {
        return localId;
    }

    public void setLocalId(String localId) {
        this.localId = localId;
    }
    
    public String getUpdatedDate() {
		return updatedDate;
	}

	public void setUpdatedDate(String updatedDate) {
		this.updatedDate = updatedDate;
	}

	public String getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}

	@Override
	public String toString() {
		return "ListPMResponse [pmId=" + pmId + ", areaName=" + areaName + ", areaId=" + areaId + ", createdBy="
				+ createdBy + ", createdDate=" + createdDate + ", pmParticipant=" + pmParticipant + ", swCandidate="
				+ swCandidate + ", pmLocationName=" + pmLocationName + ", pmOwner=" + pmOwner + ", status=" + status
				+ ", time=" + time + ", pmDate=" + pmDate + ", pmPlanDate=" + pmPlanDate + ", pmOwnerPhoneNumber="
				+ pmOwnerPhoneNumber + ", mmId=" + mmId + ", localId=" + localId + ", updatedDate=" + updatedDate
				+ ", updatedBy=" + updatedBy + ", swList=" + swList + "]";
	}
}