package id.co.telkomsigma.btpns.mprospera.response;

import java.util.ArrayList;
import java.util.List;

public class ListSDAResponse extends BaseResponse {

    private String grandTotal;
    private String currentTotal;
    private String totalPage;

    public String getTotalPage() {
        return totalPage;
    }

    public void setTotalPage(String totalPage) {
        this.totalPage = totalPage;
    }

    private List<KkHistoryResponse> kkMiskinHistory;

    public String getGrandTotal() {
        return grandTotal;
    }

    public void setGrandTotal(String grandTotal) {
        this.grandTotal = grandTotal;
    }

    public String getCurrentTotal() {
        return currentTotal;
    }

    public void setCurrentTotal(String currentTotal) {
        this.currentTotal = currentTotal;
    }

    public List<KkHistoryResponse> getKkMiskinHistory() {
        if (kkMiskinHistory == null)
            kkMiskinHistory = new ArrayList<>();
        return kkMiskinHistory;
    }

    public void setKkMiskinHistory(List<KkHistoryResponse> kkMiskinHistory) {
        this.kkMiskinHistory = kkMiskinHistory;
    }

}