package id.co.telkomsigma.btpns.mprospera.response;

import java.util.List;

public class AddMMResponse extends BaseResponse {

    private String mmId;
    private String localId;
    private List<ListPMResponse> pmListId;

    public String getMmId() {
        return mmId;
    }

    public void setMmId(String mmId) {
        this.mmId = mmId;
    }

    public List<ListPMResponse> getPmListId() {
        return pmListId;
    }

    public void setPmListId(List<ListPMResponse> pmListId) {
        this.pmListId = pmListId;
    }

    public String getLocalId() {
        return localId;
    }

    public void setLocalId(String localId) {
        this.localId = localId;
    }

    @Override
    public String toString() {
        return "AddMMResponse{" +
                "mmId='" + mmId + '\'' +
                ", localId='" + localId + '\'' +
                ", pmListId=" + pmListId +
                '}';
    }

}