package id.co.telkomsigma.btpns.mprospera.response;

import java.util.List;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;

@SuppressWarnings("ALL")
@JsonSerialize(include = JsonSerialize.Inclusion.NON_EMPTY)
public class PDKResponse extends BaseResponse {
    /*
     * untuk list PDK
     */
    private String grandTotal;
    private String currentTotal;
    private String totalPage;
    private List<ListPDKResponse> pdkList;
    private List<String> deletedPdkList;
    /*
     * untuk add PDK
     */
    private String pdkId;

    public String getGrandTotal() {
        return grandTotal;
    }

    public void setGrandTotal(String grandTotal) {
        this.grandTotal = grandTotal;
    }

    public String getCurrentTotal() {
        return currentTotal;
    }

    public void setCurrentTotal(String currentTotal) {
        this.currentTotal = currentTotal;
    }

    public String getTotalPage() {
        return totalPage;
    }

    public void setTotalPage(String totalPage) {
        this.totalPage = totalPage;
    }

    public List<ListPDKResponse> getPdkList() {
        return pdkList;
    }

    public void setPdkList(List<ListPDKResponse> pdkList) {
        this.pdkList = pdkList;
    }

    public String getPdkId() {
        return pdkId;
    }

    public void setPdkId(String pdkId) {
        this.pdkId = pdkId;
    }

    public List<String> getDeletedPdkList() {
        return deletedPdkList;
    }

    public void setDeletedPdkList(List<String> deletedPdkList) {
        this.deletedPdkList = deletedPdkList;
    }

    @Override
    public String toString() {
        return "PDKResponse [grandTotal=" + grandTotal + ", currentTotal=" + currentTotal + ", totalPage=" + totalPage
                + ", pdkList=" + pdkList + ", pdkId=" + pdkId + ", getResponseCode()=" + getResponseCode()
                + ", getResponseMessage()=" + getResponseMessage() + "]";
    }

}