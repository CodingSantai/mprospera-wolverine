package id.co.telkomsigma.btpns.mprospera.response;

import java.util.List;

public class AP3RListV2Response extends BaseResponse {

    private String grandTotal;
    private String currentTotal;
    private String totalPage;
    List<AP3RListV2> ap3rList;
    List<String> deletedAp3rList;

    public String getGrandTotal() {
        return grandTotal;
    }

    public void setGrandTotal(String grandTotal) {
        this.grandTotal = grandTotal;
    }

    public String getCurrentTotal() {
        return currentTotal;
    }

    public void setCurrentTotal(String currentTotal) {
        this.currentTotal = currentTotal;
    }

    public String getTotalPage() {
        return totalPage;
    }

    public void setTotalPage(String totalPage) {
        this.totalPage = totalPage;
    }

    public List<AP3RListV2> getAp3rList() {
        return ap3rList;
    }

    public void setAp3rList(List<AP3RListV2> ap3rList) {
        this.ap3rList = ap3rList;
    }

    public List<String> getDeletedAp3rList() {
        return deletedAp3rList;
    }

    public void setDeletedAp3rList(List<String> deletedAp3rList) {
        this.deletedAp3rList = deletedAp3rList;
    }

}