package id.co.telkomsigma.btpns.mprospera.request;

public class StackTraceRequest {

    private String declaringClass;
    private String fileName;
    private String methodName;
    private String lineNumber;

    public String getDeclaringClass() {
        return declaringClass;
    }

    public void setDeclaringClass(String declaringClass) {
        this.declaringClass = declaringClass;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public String getMethodName() {
        return methodName;
    }

    public void setMethodName(String methodName) {
        this.methodName = methodName;
    }

    public String getLineNumber() {
        return lineNumber;
    }

    public void setLineNumber(String lineNumber) {
        this.lineNumber = lineNumber;
    }

    @Override
    public String toString() {
        return "StackTraceRequest [declaringClass=" + declaringClass + ", fileName=" + fileName + ", methodName="
                + methodName + ", lineNumber=" + lineNumber + "]";
    }

}