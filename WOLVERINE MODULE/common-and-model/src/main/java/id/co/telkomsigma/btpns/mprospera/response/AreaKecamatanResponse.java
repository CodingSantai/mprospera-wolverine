package id.co.telkomsigma.btpns.mprospera.response;

import java.util.ArrayList;
import java.util.List;

public class AreaKecamatanResponse {

    private String areaName;
    private String areaId;
    private String parentAreaId;
    private String postalCode;
    List<AreaKelurahanResponse> areaListKelurahan;

    public String getAreaName() {
        return areaName;
    }

    public void setAreaName(String areaName) {
        this.areaName = areaName;
    }

    public String getAreaId() {
        return areaId;
    }

    public void setAreaId(String areaId) {
        this.areaId = areaId;
    }

    public String getParentAreaId() {
        return parentAreaId;
    }

    public void setParentAreaId(String parentAreaId) {
        this.parentAreaId = parentAreaId;
    }

    public List<AreaKelurahanResponse> getAreaListKelurahan() {
        if (areaListKelurahan == null)
            areaListKelurahan = new ArrayList<>();
        return areaListKelurahan;
    }

    public void setAreaListKelurahan(List<AreaKelurahanResponse> areaListKelurahan) {
        this.areaListKelurahan = areaListKelurahan;
    }

    public String getPostalCode() {
        return postalCode;
    }

    public void setPostalCode(String postalCode) {
        this.postalCode = postalCode;
    }

}