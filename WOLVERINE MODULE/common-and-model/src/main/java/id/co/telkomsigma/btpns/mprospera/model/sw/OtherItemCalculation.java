package id.co.telkomsigma.btpns.mprospera.model.sw;

import id.co.telkomsigma.btpns.mprospera.model.GenericModel;

import javax.persistence.*;
import java.math.BigDecimal;

/**
 * Created by Dzulfiqar on 30/05/2017.
 */
@Entity
@Table(name = "T_OTHER_ITEM_CALCULATION")
public class OtherItemCalculation extends GenericModel {

    private Long otherItemCalcId;
    private BigDecimal amount;
    private String name;
    private BigDecimal pendapatanPerbulan;
    private BigDecimal periode;
    private Long swId;

    @Id
    @Column(name = "id", nullable = false, unique = true)
    @GeneratedValue
    public Long getOtherItemCalcId() {
        return otherItemCalcId;
    }

    public void setOtherItemCalcId(Long otherItemCalcId) {
        this.otherItemCalcId = otherItemCalcId;
    }

    @Column(name = "amount")
    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    @Column(name = "name")
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Column(name = "monthly_income")
    public BigDecimal getPendapatanPerbulan() {
        return pendapatanPerbulan;
    }

    public void setPendapatanPerbulan(BigDecimal pendapatanPerbulan) {
        this.pendapatanPerbulan = pendapatanPerbulan;
    }

    @Column(name = "period")
    public BigDecimal getPeriode() {
        return periode;
    }

    public void setPeriode(BigDecimal periode) {
        this.periode = periode;
    }

    @Column(name = "sw_id")
    public Long getSwId() {
        return swId;
    }

    public void setSwId(Long swId) {
        this.swId = swId;
    }

}