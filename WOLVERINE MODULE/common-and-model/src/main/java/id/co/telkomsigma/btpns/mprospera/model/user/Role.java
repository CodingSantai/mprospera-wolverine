package id.co.telkomsigma.btpns.mprospera.model.user;

import id.co.telkomsigma.btpns.mprospera.model.menu.Menu;
import org.springframework.security.core.GrantedAuthority;
import id.co.telkomsigma.btpns.mprospera.model.GenericModel;
//import id.co.telkomsigma.btpns.mprospera.model.menu.Menu;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * Created by daniel on 3/31/15.
 */
@Entity
@Table(name = "M_ROLES")
public class Role extends GenericModel implements GrantedAuthority {
    /**
     *
     */
    private static final long serialVersionUID = 1L;
    /***************************** - Field - ****************************/
    private Long roleId;
    private String authority;
    private String description;
    private User createdBy;
    private Date createdDate;
    private User updatedBy;
    private Date updatedDate;
    private Set<Menu> menus = new HashSet<Menu>();
    /*****************************
     * - Transient Field -
     ****************************/
    private Long userIdCreated;
    private Long userIdUpdated;
    private List<String> strMenus = new ArrayList<String>();
    private Boolean canAccessWeb = true;
    private Boolean canAccessMobile = true;

    @Column(name = "CAN_ACCESS_WEB")
    public Boolean getCanAccessWeb() {
        return canAccessWeb;
    }

    @Column(name = "CAN_ACCESS_MOBILE")
    public Boolean getCanAccessMobile() {
        return canAccessMobile;
    }

    public void setCanAccessWeb(Boolean canAccessWeb) {
        this.canAccessWeb = canAccessWeb;
    }

    public void setCanAccessMobile(Boolean canAccessMobile) {
        this.canAccessMobile = canAccessMobile;
    }

    /***************************** - Constructor - ****************************/
    public Role() {
    }

    public Role(String id) {
        this.roleId = Long.parseLong(id);
    }

    public Role(Role role) {
        this.roleId = role.roleId;
        this.authority = role.authority;
        this.description = role.description;
        this.createdBy = role.createdBy;
        this.createdDate = role.createdDate;
        this.updatedBy = role.updatedBy;
        this.updatedDate = role.updatedDate;
    }

    /*****************************
     * - Getter Method -
     ****************************/
    @Id
    @Column(name = "id", unique = true, nullable = false)
    @GeneratedValue
    public Long getRoleId() {
        return roleId;
    }

    @Override
    @Column(name = "ROLE_NAME", length = MAX_LENGTH_AUTHORITY, unique = true)
    public String getAuthority() {
        return authority;
    }

    @Column(name = "DESCRIPTION", length = MAX_LENGTH_DESCRIPTION)
    public String getDescription() {
        return description;
    }

    @ManyToOne(targetEntity = User.class, fetch = FetchType.LAZY)
    @JoinColumn(name = "CREATED_BY", nullable = false)
    public User getCreatedBy() {
        return createdBy;
    }

    @Column(name = "CREATED_DT")
    public Date getCreatedDate() {
        return createdDate;
    }

    @ManyToOne(targetEntity = User.class, fetch = FetchType.LAZY)
    @JoinColumn(name = "UPDATE_BY", nullable = false)
    public User getUpdatedBy() {
        return updatedBy;
    }

    @Column(name = "UPDATE_DT")
    public Date getUpdatedDate() {
        return updatedDate;
    }

    @Transient
    public Long getUserIdCreated() {
        return userIdCreated;
    }

    @Transient
    public Long getUserIdUpdated() {
        return userIdUpdated;
    }

    /*****************************
     * - Setter Method -
     ****************************/

    public void setRoleId(Long id) {
        this.roleId = id;
    }

    public void setAuthority(String authority) {
        this.authority = authority;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public void setCreatedBy(User createdBy) {
        this.createdBy = createdBy;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    public void setUpdatedBy(User updatedBy) {
        this.updatedBy = updatedBy;
    }

    public void setUpdatedDate(Date updatedDate) {
        this.updatedDate = updatedDate;
    }

    public void setUserIdCreated(Long userIdCreated) {
        this.userIdCreated = userIdCreated;
    }

    public void setUserIdUpdated(Long userIdUpdated) {
        this.userIdUpdated = userIdUpdated;
    }

    /*****************************
     * - Other Method -
     ****************************/

    public enum System {
        ADMIN("ADMIN"), CS("CS"), CUSTOMER("CUSTOMER"), SYSTEM("SYSTEM");

        private String authority;

        System(String authority) {
            this.authority = authority;
        }

        public String getAuthority() {
            return this.authority;
        }
    }

   @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(name = "M_ROLE_MENUS", joinColumns = {
            @JoinColumn(name = "roleId")}, inverseJoinColumns = @JoinColumn(name = "menuId"))
    public Set<Menu> getMenus() {
        return menus;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((authority == null) ? 0 : authority.hashCode());
        result = prime * result + ((description == null) ? 0 : description.hashCode());
        result = prime * result + ((roleId == null) ? 0 : roleId.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        Role other = (Role) obj;
        if (authority == null) {
            if (other.authority != null)
                return false;
        } else if (!authority.equals(other.authority))
            return false;
        if (description == null) {
            if (other.description != null)
                return false;
        } else if (!description.equals(other.description))
            return false;
        if (roleId == null) {
            if (other.roleId != null)
                return false;
        } else if (!roleId.equals(other.roleId))
            return false;
        return true;
    }

    public void setMenus(Set<Menu> menus) {
        this.menus = menus;
    }

    @Transient
    public List<String> getStrMenus() {
        return strMenus;
    }

    public void setStrMenus(List<String> strMenus) {
        this.strMenus = strMenus;
    }

    public void addMenu(Menu menu) {
        if (menus == null) {
            menus = new HashSet<Menu>();
        }
        menus.add(menu);
    }

}