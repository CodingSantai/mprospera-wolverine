package id.co.telkomsigma.btpns.mprospera.request;

import java.util.List;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;

@SuppressWarnings("ALL")
@JsonSerialize(include = JsonSerialize.Inclusion.NON_EMPTY)
public class AddSDARequest extends BaseRequest {

    private String username;
    private String imei;
    private String sessionKey;
    private String areaCategory;
    private String parentAreaId;
    private String areaId;
    private String areaName;
    private String countKkMiskin;
    private String sdaId;
    private String action;
    private String longitude;
    private String latitude;
    private String pejabat;
    private String jabatan;
    private String phoneNumber;
    private String localId;
    private List<AddSDAKelList> sdaKelList;

    public String getLongitude() {
        return longitude;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public String getSdaId() {
        return sdaId;
    }

    public void setSdaId(String sdaId) {
        this.sdaId = sdaId;
    }

    public String getAction() {
        return action;
    }

    public void setAction(String action) {
        this.action = action;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getImei() {
        return imei;
    }

    public void setImei(String imei) {
        this.imei = imei;
    }

    public String getSessionKey() {
        return sessionKey;
    }

    public void setSessionKey(String sessionKey) {
        this.sessionKey = sessionKey;
    }

    public String getAreaCategory() {
        return areaCategory;
    }

    public void setAreaCategory(String areaCategory) {
        this.areaCategory = areaCategory;
    }

    public String getParentAreaId() {
        return parentAreaId;
    }

    public void setParentAreaId(String parentAreaId) {
        this.parentAreaId = parentAreaId;
    }

    public String getAreaId() {
        return areaId;
    }

    public void setAreaId(String areaId) {
        this.areaId = areaId;
    }

    public String getAreaName() {
        return areaName;
    }

    public void setAreaName(String areaName) {
        this.areaName = areaName;
    }

    public String getCountKkMiskin() {
        return countKkMiskin;
    }

    public void setCountKkMiskin(String countKkMiskin) {
        this.countKkMiskin = countKkMiskin;
    }

    public String getPejabat() {
        return pejabat;
    }

    public void setPejabat(String pejabat) {
        this.pejabat = pejabat;
    }

    public String getJabatan() {
        return jabatan;
    }

    public void setJabatan(String jabatan) {
        this.jabatan = jabatan;
    }

    public List<AddSDAKelList> getSdaKelList() {
        return sdaKelList;
    }

    public void setSdaKelList(List<AddSDAKelList> sdaKelList) {
        this.sdaKelList = sdaKelList;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getLocalId() {
        return localId;
    }

    public void setLocalId(String localId) {
        this.localId = localId;
    }

    @Override
    public String toString() {
        return "AddSDARequest{" +
                "username='" + username + '\'' +
                ", imei='" + imei + '\'' +
                ", sessionKey='" + sessionKey + '\'' +
                ", areaCategory='" + areaCategory + '\'' +
                ", parentAreaId='" + parentAreaId + '\'' +
                ", areaId='" + areaId + '\'' +
                ", areaName='" + areaName + '\'' +
                ", countKkMiskin='" + countKkMiskin + '\'' +
                ", sdaId='" + sdaId + '\'' +
                ", action='" + action + '\'' +
                ", longitude='" + longitude + '\'' +
                ", latitude='" + latitude + '\'' +
                ", pejabat='" + pejabat + '\'' +
                ", jabatan='" + jabatan + '\'' +
                ", phoneNumber='" + phoneNumber + '\'' +
                ", localId='" + localId + '\'' +
                ", sdaKelList=" + sdaKelList +
                '}';
    }

}