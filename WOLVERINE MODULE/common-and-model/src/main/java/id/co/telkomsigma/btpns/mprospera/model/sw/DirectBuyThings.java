package id.co.telkomsigma.btpns.mprospera.model.sw;

import java.math.BigDecimal;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import id.co.telkomsigma.btpns.mprospera.model.GenericModel;

@Entity
@Table(name = "T_DIRECT_BUY")
public class DirectBuyThings extends GenericModel {

    private Long directBuyId;
    private String namaBarang;
    private BigDecimal price;
    private Integer frequency;
    private BigDecimal totalPrice;
    private Integer index;
    private Integer totalItem;
    private Integer type;
    private BigDecimal sellingPrice;
    private SurveyWawancara swId;

    @Id
    @Column(name = "id", nullable = false, unique = true)
    @GeneratedValue
    public Long getDirectBuyId() {
        return directBuyId;
    }

    public void setDirectBuyId(Long directBuyId) {
        this.directBuyId = directBuyId;
    }

    @Column(name = "nama_barang")
    public String getNamaBarang() {
        return namaBarang;
    }

    public void setNamaBarang(String namaBarang) {
        this.namaBarang = namaBarang;
    }

    @Column(name = "price")
    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    @Column(name = "frequency")
    public Integer getFrequency() {
        return frequency;
    }

    public void setFrequency(Integer frequency) {
        this.frequency = frequency;
    }

    @Column(name = "total_price")
    public BigDecimal getTotalPrice() {
        return totalPrice;
    }

    public void setTotalPrice(BigDecimal totalPrice) {
        this.totalPrice = totalPrice;
    }

    @ManyToOne(fetch = FetchType.EAGER, targetEntity = SurveyWawancara.class)
    @JoinColumn(name = "sw_id", referencedColumnName = "id", nullable = true)
    public SurveyWawancara getSwId() {
        return swId;
    }

    public void setSwId(SurveyWawancara swId) {
        this.swId = swId;
    }

    @Column(name = "index_item")
    public Integer getIndex() {
        return index;
    }

    public void setIndex(Integer index) {
        this.index = index;
    }

    @Column(name = "total_item")
    public Integer getTotalItem() {
        return totalItem;
    }

    public void setTotalItem(Integer totalItem) {
        this.totalItem = totalItem;
    }

    @Column(name = "type")
    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    @Column(name = "selling_price")
    public BigDecimal getSellingPrice() {
        return sellingPrice;
    }

    public void setSellingPrice(BigDecimal sellingPrice) {
        this.sellingPrice = sellingPrice;
    }

}