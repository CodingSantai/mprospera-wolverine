package id.co.telkomsigma.btpns.mprospera.model.security;

import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import id.co.telkomsigma.btpns.mprospera.model.GenericModel;

@Entity
@Table(name = "M_AUDIT_LOG")
public class AuditLog extends GenericModel {

    /**
     *
     */
    private static final long serialVersionUID = 1L;
    private Long auditId;
    private Integer activityType;
    private String description;
    private String createdBy;
    private Date createdDate;
    private String reffNo;
    /*****************************
     * - ACTIVITY TYPE -
     ****************************/
    public static final int LOGIN_LOGOUT = 1;
    public static final int VIEW_PAGE = 2;
    public static final int SAVE_OR_UPDATE = 3;
    public static final int DELETE_CACHE = 4;
    public static final int LOGIN_LOGOUT_DEVICE = 5;
    public static final int MODIF_SDA = 6;
    public static final int MODIF_PM = 7;
    public static final int MODIF_SW = 8;
    public static final int MODIF_SENTRA = 9;
    public static final int MODIF_SENTRA_GROUP = 10;
    public static final int MODIF_PDK = 11;
    public static final int MODIF_CUSTOMER = 12;
    public static final int MODIF_LOAN = 13;
    public static final int MODIF_MM = 14;
    public static final int MODIF_SURVEY = 15;
    public static final int TOKEN_REQUEST = 16;
    public static final int PASS_RESET = 17;
    public static final int ASSIGN_PS = 18;
    public static final int GENERATE_REPORT = 19;
    public static final int MODIF_AP3R = 20;

    @Column(name = "REFF_NO", nullable = false, length = MAX_LENGTH_REFF_NO)
    public String getReffNo() {
        return reffNo;
    }

    public void setReffNo(String reffNo) {
        this.reffNo = reffNo;
    }

    @Id
    @Column(name = "id", nullable = false, unique = true)
    @GeneratedValue
    public Long getAuditId() {
        return auditId;
    }

    @Column(name = "ACTIVITY_TYPE", nullable = false)
    public Integer getActivityType() {
        return activityType;
    }

    @Column(name = "DESCRIPTION", nullable = false, length = MAX_LENGTH_DESCRIPTION)
    public String getDescription() {
        return description;
    }

    public void setAuditId(Long auditId) {
        this.auditId = auditId;
    }

    public void setActivityType(Integer activity_type) {
        this.activityType = activity_type;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Column(name = "CREATED_BY", nullable = false)
    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    @Column(name = "CREATED_DT", nullable = false)
    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

}