package id.co.telkomsigma.btpns.mprospera.response;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import java.util.List;

@SuppressWarnings("ALL")
@JsonSerialize(include = JsonSerialize.Inclusion.NON_EMPTY)
public class AreaProvinceResponse {

    private String areaName;
    private String areaId;
    private String parentAreaId;
    private String postalCode;
    List<AreaDistrictResponse> areaListKota;

    public String getAreaName() {
        return areaName;
    }

    public void setAreaName(String areaName) {
        this.areaName = areaName;
    }

    public String getAreaId() {
        return areaId;
    }

    public void setAreaId(String areaId) {
        this.areaId = areaId;
    }

    public String getParentAreaId() {
        return parentAreaId;
    }

    public void setParentAreaId(String parentAreaId) {
        this.parentAreaId = parentAreaId;
    }

    public String getPostalCode() {
        return postalCode;
    }

    public void setPostalCode(String postalCode) {
        this.postalCode = postalCode;
    }

    public List<AreaDistrictResponse> getAreaListKota() {
        return areaListKota;
    }

    public void setAreaListKota(List<AreaDistrictResponse> areaListKota) {
        this.areaListKota = areaListKota;
    }

}