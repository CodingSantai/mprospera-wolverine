package id.co.telkomsigma.btpns.mprospera.response;

import java.util.List;

public class MMResponse {

    private String mmId;
    private String areaName;
    private String areaId;
    private String createdBy;
    private String createdDate;
    private String mmParticipant;
    private String mmLocationName;
    private String mmOwner;
    private String pmCandidate;
    private String updatedBy;
    private String updatedDate;
    private List<ListPMResponse> pmList;

    public String getMmOwner() {
        return mmOwner;
    }

    public void setMmOwner(String mmOwner) {
        this.mmOwner = mmOwner;
    }

    public String getMmId() {
        return mmId;
    }

    public void setMmId(String mmId) {
        this.mmId = mmId;
    }

    public String getAreaName() {
        return areaName;
    }

    public void setAreaName(String areaName) {
        this.areaName = areaName;
    }

    public String getAreaId() {
        return areaId;
    }

    public void setAreaId(String areaId) {
        this.areaId = areaId;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public String getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(String createdDate) {
        this.createdDate = createdDate;
    }

    public String getMmLocationName() {
        return mmLocationName;
    }

    public void setMmLocationName(String mmLocationName) {
        this.mmLocationName = mmLocationName;
    }

    public String getPmCandidate() {
        return pmCandidate;
    }

    public void setPmCandidate(String pmCandidate) {
        this.pmCandidate = pmCandidate;
    }

    public List<ListPMResponse> getPmList() {
        return pmList;
    }

    public void setPmList(List<ListPMResponse> pmList) {
        this.pmList = pmList;
    }

    public String getMmParticipant() {
        return mmParticipant;
    }

    public void setMmParticipant(String mmParticipant) {
        this.mmParticipant = mmParticipant;
    }
    

	public String getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}

	public String getUpdatedDate() {
		return updatedDate;
	}

	public void setUpdatedDate(String updatedDate) {
		this.updatedDate = updatedDate;
	}

	@Override
	public String toString() {
		return "MMResponse [mmId=" + mmId + ", areaName=" + areaName + ", areaId=" + areaId + ", createdBy=" + createdBy
				+ ", createdDate=" + createdDate + ", mmParticipant=" + mmParticipant + ", mmLocationName="
				+ mmLocationName + ", mmOwner=" + mmOwner + ", pmCandidate=" + pmCandidate + ", updatedBy=" + updatedBy
				+ ", updatedDate=" + updatedDate + ", pmList=" + pmList + "]";
	}

}