package id.co.telkomsigma.btpns.mprospera.model.sw;

import id.co.telkomsigma.btpns.mprospera.model.GenericModel;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "T_SW_ID_PHOTO")
public class SwIdPhoto extends GenericModel {

    /**
     *
     */
    private static final long serialVersionUID = 6559145228034293868L;
    private Long id;
    private SurveyWawancara swId;
    private byte[] idPhoto;

    @Id
    @Column(name = "id", nullable = false, unique = true)
    @GeneratedValue
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @ManyToOne(fetch = FetchType.EAGER, targetEntity = SurveyWawancara.class)
    @JoinColumn(name = "sw_id", referencedColumnName = "id", nullable = true)
    public SurveyWawancara getSwId() {
        return swId;
    }

    public void setSwId(SurveyWawancara swId) {
        this.swId = swId;
    }

    @Column(name = "id_photo", nullable = true)
    public byte[] getIdPhoto() {
        return idPhoto;
    }

    public void setIdPhoto(byte[] idPhoto) {
        this.idPhoto = idPhoto;
    }

}