import id.co.telkomsigma.btpns.mprospera.controller.webservice.WebServiceSyncAP3RV2Controller;
import id.co.telkomsigma.btpns.mprospera.model.sw.AP3R;
import id.co.telkomsigma.btpns.mprospera.model.terminal.Terminal;
import id.co.telkomsigma.btpns.mprospera.model.user.User;
import id.co.telkomsigma.btpns.mprospera.request.AP3RRequest;
import id.co.telkomsigma.btpns.mprospera.response.AP3RListV2Response;
import id.co.telkomsigma.btpns.mprospera.service.*;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Bean;
import org.springframework.data.domain.Page;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.test.context.junit4.SpringRunner;

import java.text.ParseException;
import java.util.Locale;

import static org.junit.Assert.assertEquals;

@RunWith(SpringRunner.class)
public class SyncAP3RV2ControllerTest {

    @TestConfiguration
    static class SyncAP3RV2ControllerTestContextConfiguration {
        @Bean
        public WebServiceSyncAP3RV2Controller webServiceSyncAP3RV2Controller() {
            return new WebServiceSyncAP3RV2Controller();
        }

        @Bean
        protected Locale locale() {
            return new Locale("id", "ID");
        }
    }

    @Autowired
    private WebServiceSyncAP3RV2Controller webServiceSyncAP3RV2Controller;

    @MockBean
    private WSValidationService wsValidationService;

    @MockBean
    private TerminalActivityService terminalActivityService;

    @MockBean
    private ThreadPoolTaskExecutor threadPoolTaskExecutor;

    @MockBean
    private TerminalService terminalService;

    @MockBean
    private UserService userService;

    @MockBean
    private AP3RService ap3RService;

    @MockBean
    private SWService swService;

    @MockBean
    private CustomerService customerService;

    @MockBean
    private LoanService loanService;

    @MockBean
    private AreaService areaService;

    String username = "w0211f";
    String imei = "358525070497940";
    String apkVersion = "3.70";
    String sessionKey = "0beba6ac-2b04-4889-866f-b36dea07aff6";
    String page = "1";
    String countData = "500";
    String startLookupDate = "";
    String endLookupDate = "";
    AP3RListV2Response ap3RListV2Response = new AP3RListV2Response();
    User user = new User();
    Page<AP3R> ap3rList;
    Terminal terminal = new Terminal();

    @Before
    public void setUp() {
        ap3RListV2Response.setResponseCode("00");
        user.setUserId(Long.parseLong("142432"));
        user.setRoleUser("1");
        Mockito.when(wsValidationService.wsValidation(username, imei, sessionKey, apkVersion)).thenReturn("00");
        Mockito.when(userService.findUserByUsername(username)).thenReturn(user);
        try {
            Mockito.when(ap3RService.getAp3rByUserMS(username, page, countData, startLookupDate, endLookupDate)).thenReturn(ap3rList);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        Mockito.when(terminalService.loadTerminalByImei(imei)).thenReturn(terminal);
    }

    @Test
    public void WhenSyncAP3RV2_ThenReturnSuccess() {
        AP3RRequest swRequest = new AP3RRequest();
        swRequest.setImei("358525070497940");
        swRequest.setUsername("w0211f");
        swRequest.setStartLookupDate("");
        swRequest.setEndLookupDate("");
        swRequest.setPage(page);
        swRequest.setGetCountData(countData);
        swRequest.setSessionKey("0beba6ac-2b04-4889-866f-b36dea07aff6");
        try {
            AP3RListV2Response result = webServiceSyncAP3RV2Controller.doAP3RListV2(swRequest, apkVersion);
            assertEquals(result.getResponseCode(), ap3RListV2Response.getResponseCode());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}