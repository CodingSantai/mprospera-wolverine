package id.co.telkomsigma.btpns.mprospera.controller.webservice;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import id.co.telkomsigma.btpns.mprospera.service.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import id.co.telkomsigma.btpns.mprospera.constant.WebGuiConstant;
import id.co.telkomsigma.btpns.mprospera.controller.GenericController;
import id.co.telkomsigma.btpns.mprospera.model.messageLogs.MessageLogs;
import id.co.telkomsigma.btpns.mprospera.model.sda.AreaKelurahan;
import id.co.telkomsigma.btpns.mprospera.model.sda.AreaKelurahanDetails;
import id.co.telkomsigma.btpns.mprospera.model.sda.AreaSubDistrict;
import id.co.telkomsigma.btpns.mprospera.model.sda.AreaSubDistrictDetails;
import id.co.telkomsigma.btpns.mprospera.model.security.AuditLog;
import id.co.telkomsigma.btpns.mprospera.model.terminal.Terminal;
import id.co.telkomsigma.btpns.mprospera.model.terminal.TerminalActivity;
import id.co.telkomsigma.btpns.mprospera.request.AddSDAKelList;
import id.co.telkomsigma.btpns.mprospera.request.AddSDARequest;
import id.co.telkomsigma.btpns.mprospera.response.AddSDAKelListResponse;
import id.co.telkomsigma.btpns.mprospera.response.AddSDAResponse;
import id.co.telkomsigma.btpns.mprospera.util.JsonUtils;
import id.co.telkomsigma.btpns.mprospera.util.TransactionIdGenerator;

@Controller
public class WebServiceAddSDAController extends GenericController {

    @Autowired
    private WSValidationService wsValidationService;

    @Autowired
    private SDAService sdaService;

    @Autowired
    private AreaService areaService;

    @Autowired
    private AuditLogService auditLogService;

    @Autowired
    private TerminalActivityService terminalActivityService;

    @Autowired
    private ThreadPoolTaskExecutor threadPoolTaskExecutor;

    @Autowired
    private TerminalService terminalService;

    JsonUtils jsonUtils = new JsonUtils();

    // Submit Data SDA Kecamatan API
    @RequestMapping(value = WebGuiConstant.SDA_KEC_SUBMIT_DATA_REQUEST, method = {RequestMethod.POST})
    public @ResponseBody
    AddSDAResponse doAdd(@RequestBody String requestString,
                         @PathVariable("apkVersion") String apkVersion) throws Exception {

        Object obj = new JsonUtils().fromJson(requestString, AddSDARequest.class);
        final AddSDARequest request = (AddSDARequest) obj;
        final AddSDAResponse responseCode = new AddSDAResponse();
        responseCode.setResponseCode(WebGuiConstant.RC_SUCCESS);

        try {
            log.info("addSdaKec INCOMING MESSAGE : " + jsonUtils.toJson(request));
            // username , imei, session key, apk version validation
            String validation = wsValidationService.wsValidation(request.getUsername(), request.getImei(),
                    request.getSessionKey(), apkVersion);
            if (!validation.equals(WebGuiConstant.RC_SUCCESS)) {
                responseCode.setResponseCode(validation);
                String label = getMessage("webservice.rc.label." + responseCode.getResponseCode());
                responseCode.setResponseMessage(label);
                log.error("Validation Failed for username : " + request.getUsername() + " ,imei : " + request.getImei() + " ,sessionKey : " + request.getSessionKey());
            } else {
                log.debug("validation using " + request.getUsername() + " success");
                AreaSubDistrict kec = sdaService.findSubDistrictById(request.getAreaId());
                if (kec == null) {
                    log.error("Data kecamatan yang dikirim tidak valid");
                    responseCode.setResponseCode(WebGuiConstant.RC_INVALID_KECAMATAN);
                    String label = getMessage("webservice.rc.label." + responseCode.getResponseCode());
                    responseCode.setResponseMessage(label);
                    return responseCode;
                }
                AreaSubDistrictDetails sdaKec = new AreaSubDistrictDetails();
                if ("delete".equals(request.getAction()) || "update".equals(request.getAction())) {
                    sdaKec.setAreaDetailId(Long.parseLong(request.getSdaId()));
                    sdaKec.setUpdatedDate(new Date());
                }
                if ("delete".equals(request.getAction()))
                    sdaKec.setIsDeleted(true);
                sdaKec.setAreaSubDistrict(kec);
                sdaKec.setCountKKMiskin(request.getCountKkMiskin());// Jumlah KK Miskin
                sdaKec.setCreatedAt(request.getImei());
                sdaKec.setCreatedBy(request.getUsername());
                sdaKec.setCreatedDate(new Date());
                sdaKec.setPejabat(request.getPejabat());// Pejabat yang ditemui
                sdaKec.setJabatan(request.getJabatan());// Jabatan pejabat yang ditemui
                sdaKec.setPhoneNumber(request.getPhoneNumber());
                sdaKec.setLongitude(request.getLongitude());
                sdaKec.setLatitude(request.getLatitude());
                if (("delete".equals(request.getAction()) || "update".equals(request.getAction()))
                        && !sdaService.isValidSda(request.getSdaId(), request.getAreaCategory())) {
                    responseCode.setResponseCode(WebGuiConstant.RC_UNKNOWN_SDA_ID);
                    log.error("ID SDA yang dikirim tidak ditemukan");
                } else {
                    try {
                        sdaService.saveSubDistrictDetails(sdaKec);
                        log.debug("SubDistrictDetails SAVED SUCCESSFULLY");
                    } catch (Exception e) {
                        log.error("FAIL SAVING SubDistrictDetails");
                        log.error("ERROR SAVE SUB DISTRIC DETAILS on Class WebServiceAddSDAController :"
                                + e.getMessage());
                    }
                    responseCode.setSdaId("" + sdaKec.getAreaDetailId());
                    log.debug("save area sub district details, area Id :  " + request.getAreaId());
                }
                List<AddSDAKelListResponse> sdaKelListResponse = new ArrayList<>();
                for (AddSDAKelList kelList : request.getSdaKelList()) {
                    AreaKelurahanDetails kelSda = new AreaKelurahanDetails();
                    AreaKelurahan kel = areaService.findKelurahanById(kelList.getAreaId());
                    if (kel == null) {
                        log.error("Data Kelurahan tidak valid");
                        responseCode.setResponseCode(WebGuiConstant.RC_INVALID_KELURAHAN);
                        String label = getMessage("webservice.rc.label." + responseCode.getResponseCode());
                        responseCode.setResponseMessage(label);
                        return responseCode;
                    }
                    if ("delete".equals(kelList.getAction()) || "update".equals(kelList.getAction())) {
                        kelSda.setAreaDetailId(Long.parseLong(kelList.getSdaId()));
                        kelSda.setUpdatedDate(new Date());
                    }
                    if ("delete".equals(kelList.getAction()))
                        kelSda.setIsDeleted(true);
                    kelSda.setAreaKelurahan(kel);
                    kelSda.setCountKKMiskin(kelList.getCountKkMiskin());
                    kelSda.setCreatedAt(request.getImei());
                    kelSda.setCreatedBy(request.getUsername());
                    kelSda.setCreatedDate(new Date());
                    kelSda.setPejabat(kelList.getPejabat());
                    kelSda.setJabatan(kelList.getJabatan());
                    kelSda.setPhoneNumber(kelList.getPhoneNumber());
                    kelSda.setLongitude(request.getLongitude());
                    kelSda.setLatitude(request.getLatitude());
                    if (("delete".equals(kelList.getAction()) || "update".equals(kelList.getAction()))
                            && !sdaService.isValidSda(kelList.getSdaId(), kelList.getAreaCategory())) {
                        responseCode.setResponseCode(WebGuiConstant.RC_UNKNOWN_SDA_ID);
                        log.error("ID SDA yang dikirim tidak ditemukan");
                    } else {
                        sdaService.saveKelurahanDetails(kelSda);
                        AddSDAKelListResponse sdaKelResponse = new AddSDAKelListResponse();
                        sdaKelResponse.setSdaId(kelSda.getAreaDetailId().toString());
                        sdaKelListResponse.add(sdaKelResponse);
                        sdaKelResponse.setLocalId(kelList.getLocalId());
                        log.debug("save area kelurahan details, area Id :  " + kelList.getAreaId());
                    }
                }
                responseCode.setSdaKelList(sdaKelListResponse);
                responseCode.setLocalId(request.getLocalId());
                String label = getMessage("webservice.rc.label." + responseCode.getResponseCode());
                responseCode.setResponseMessage(label);
            }
        } catch (Exception e) {
            log.error("addSdaKec ERROR METHOD doAdd,Class WebServiceAddSDAController :" + e.getMessage(), e);
            responseCode.setResponseCode(WebGuiConstant.RC_GENERAL_ERROR);
            responseCode.setResponseMessage("addSdaKec error: " + e.getMessage());
        } finally {
            try {
                log.debug("Trying to CREATE Terminal Activity....");
                threadPoolTaskExecutor.execute(new Runnable() {
                    @Override
                    public void run() {
                        Terminal terminal = terminalService.loadTerminalByImei(request.getImei());
                        TerminalActivity terminalActivity = new TerminalActivity();
                        // Logging to terminal activity
                        terminalActivity.setTerminalActivityId(TransactionIdGenerator.generateTransactionId());
                        terminalActivity.setActivityType(TerminalActivity.ACTIVITY_MODY_addSdaKec);
                        terminalActivity.setCreatedDate(new Date());
                        terminalActivity.setReffNo(request.getRetrievalReferenceNumber().trim());
                        terminalActivity.setSessionKey(request.getSessionKey());
                        terminalActivity.setTerminal(terminal);
                        terminalActivity.setUsername(request.getUsername().trim());
                        List<MessageLogs> messageLogs = new ArrayList<>();
                        terminalActivityService.saveTerminalActivityAndMessageLogs(terminalActivity, messageLogs);
                        log.debug("TERMINAL ACTIVITY LOGGING SUCCESS...");
                        log.debug("Updating Terminal Activity");
                    }
                });
                log.info("addSdaKec RESPONSE MESSAGE : " + jsonUtils.toJson(responseCode));
            } catch (Exception e) {
                log.error("addSdaKec TerminalActivity ERROR :" + e.getMessage());
            }
        }
        return responseCode;
    }

    // Submit Data SDA Kelurahan API
    @RequestMapping(value = WebGuiConstant.SDA_KEL_SUBMIT_DATA_REQUEST, method = {RequestMethod.POST})
    public @ResponseBody
    AddSDAResponse doAddKel(@RequestBody String requestString,
                            @PathVariable("apkVersion") String apkVersion) throws Exception {

        Object obj = new JsonUtils().fromJson(requestString, AddSDARequest.class);
        final AddSDARequest request = (AddSDARequest) obj;
        final AddSDAResponse responseCode = new AddSDAResponse();
        responseCode.setResponseCode(WebGuiConstant.RC_SUCCESS);
        try {
            log.info("addSdaKel INCOMING MESSAGE : " + jsonUtils.toJson(request));
            // validation
            String validation = wsValidationService.wsValidation(request.getUsername(), request.getImei(), request.getSessionKey(), apkVersion);
            if (!validation.equals(WebGuiConstant.RC_SUCCESS)) {
                responseCode.setResponseCode(validation);
                String label = getMessage("webservice.rc.label." + responseCode.getResponseCode());
                responseCode.setResponseMessage(label);
                log.error("Validation Failed for username : " + request.getUsername() + " ,imei : " + request.getImei() + " ,sessionKey : " + request.getSessionKey());
            } else {
                log.debug("validation using " + request.getUsername() + " success");
                AreaKelurahan kel = areaService.findKelurahanById(request.getAreaId());
                if (kel == null) {
                    responseCode.setResponseCode(WebGuiConstant.RC_INVALID_KELURAHAN);
                    String label = getMessage("webservice.rc.label." + responseCode.getResponseCode());
                    responseCode.setResponseMessage(label);
                    return responseCode;
                }
                AreaKelurahanDetails sdaKel = new AreaKelurahanDetails();
                if ("delete".equals(request.getAction()) || "update".equals(request.getAction())) {
                    sdaKel.setAreaDetailId(Long.parseLong(request.getSdaId()));
                    sdaKel.setUpdatedDate(new Date());
                }
                if ("delete".equals(request.getAction()))
                    sdaKel.setIsDeleted(true);
                sdaKel.setAreaKelurahan(kel);
                sdaKel.setCountKKMiskin(request.getCountKkMiskin());
                sdaKel.setCreatedAt(request.getImei());
                sdaKel.setCreatedBy(request.getUsername());
                sdaKel.setCreatedDate(new Date());
                sdaKel.setPejabat(request.getPejabat());
                sdaKel.setJabatan(request.getJabatan());
                sdaKel.setPhoneNumber(request.getPhoneNumber());
                sdaKel.setLatitude(request.getLatitude());
                sdaKel.setLongitude(request.getLongitude());
                if (("delete".equals(request.getAction()) || "update".equals(request.getAction()))
                        && !sdaService.isValidSda(request.getSdaId(), request.getAreaCategory())) {
                    responseCode.setResponseCode(WebGuiConstant.RC_UNKNOWN_SDA_ID);
                    log.error("ID SDA tidak ditemukan");
                } else {
                    try {
                        sdaService.saveKelurahanDetails(sdaKel);
                    } catch (Exception e) {
                        log.error("FAILED SAVE KELURAHAN DETAILS " + e.getMessage());
                    }
                    responseCode.setSdaId("" + sdaKel.getAreaDetailId());
                    responseCode.setLocalId(request.getLocalId());
                    log.debug("save area kelurahan details, area Id :  " + request.getAreaId());
                }
            }
            String label = getMessage("webservice.rc.label." + responseCode.getResponseCode());
            responseCode.setResponseMessage(label);
        } catch (Exception e) {
            log.error("addSdaKel ERROR METHOD doAddKel, Class WebServiceAddSDAController " + e.getMessage(), e);
            responseCode.setResponseCode(WebGuiConstant.RC_GENERAL_ERROR);
            responseCode.setResponseMessage("addSdaKel error: " + e.getMessage());
        } finally {
            try {
                log.debug("enter terminal activity");
                threadPoolTaskExecutor.execute(new Runnable() {
                    @Override
                    public void run() {
                        Terminal terminal = terminalService.loadTerminalByImei(request.getImei());
                        TerminalActivity terminalActivity = new TerminalActivity();
                        // Logging to terminal activity
                        terminalActivity.setTerminalActivityId(TransactionIdGenerator.generateTransactionId());
                        terminalActivity.setActivityType(TerminalActivity.ACTIVITY_MODY_addSdaKel);
                        terminalActivity.setCreatedDate(new Date());
                        terminalActivity.setReffNo(request.getRetrievalReferenceNumber().trim());
                        terminalActivity.setSessionKey(request.getSessionKey());
                        terminalActivity.setTerminal(terminal);
                        terminalActivity.setUsername(request.getUsername().trim());
                        List<MessageLogs> messageLogs = new ArrayList<>();
                        terminalActivityService.saveTerminalActivityAndMessageLogs(terminalActivity, messageLogs);
                        log.debug("TERMINAL ACTIVITY LOGGING SUCCESS...");
                        log.debug("Updating Terminal Activity");
                        AuditLog auditLog = new AuditLog();
                        auditLog.setActivityType(AuditLog.MODIF_SDA);
                        auditLog.setCreatedBy(request.getUsername());
                        auditLog.setCreatedDate(new Date());
                        auditLog.setDescription(request.toString() + " | " + responseCode.toString());
                        auditLog.setReffNo(request.getRetrievalReferenceNumber());
                        auditLogService.insertAuditLog(auditLog);
                        log.debug("AUDIT LOG LOGGING SUCCESS...");
                        log.debug("Updating AUDIT LOG");
                    }
                });
                log.info("addSdaKel RESPONSE MESSAGE : " + jsonUtils.toJson(responseCode));
            } catch (Exception e) {
                log.error("addSdaKel saveTerminalActivityAndMessageLogs error: " + e.getMessage());
            }
        }
        return responseCode;
    }

}