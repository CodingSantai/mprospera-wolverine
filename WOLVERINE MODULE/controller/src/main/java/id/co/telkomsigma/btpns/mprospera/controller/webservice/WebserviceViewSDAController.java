package id.co.telkomsigma.btpns.mprospera.controller.webservice;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import id.co.telkomsigma.btpns.mprospera.constant.WebGuiConstant;
import id.co.telkomsigma.btpns.mprospera.controller.GenericController;
import id.co.telkomsigma.btpns.mprospera.model.messageLogs.MessageLogs;
import id.co.telkomsigma.btpns.mprospera.model.sda.AreaDistrict;
import id.co.telkomsigma.btpns.mprospera.model.sda.AreaKelurahanDetails;
import id.co.telkomsigma.btpns.mprospera.model.sda.AreaSubDistrictDetails;
import id.co.telkomsigma.btpns.mprospera.model.terminal.Terminal;
import id.co.telkomsigma.btpns.mprospera.model.terminal.TerminalActivity;
import id.co.telkomsigma.btpns.mprospera.model.user.User;
import id.co.telkomsigma.btpns.mprospera.request.ListSDARequest;
import id.co.telkomsigma.btpns.mprospera.response.KkHistoryResponse;
import id.co.telkomsigma.btpns.mprospera.response.ListSDAResponse;
import id.co.telkomsigma.btpns.mprospera.service.*;
import id.co.telkomsigma.btpns.mprospera.util.JsonUtils;
import id.co.telkomsigma.btpns.mprospera.util.PercentageSynchronizer;
import id.co.telkomsigma.btpns.mprospera.util.TransactionIdGenerator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller("webserviceViewSDAController")
public class WebserviceViewSDAController extends GenericController {

    @Autowired
    private TerminalService terminalService;

    @Autowired
    private TerminalActivityService terminalActivityService;

    @Autowired
    private WSValidationService wsValidationService;

    @Autowired
    private AreaService areaService;

    @Autowired
    private SyncAreaService syncAreaService;

    @Autowired
    private SDAService sdaService;

    @Autowired
    private UserService userService;

    @Autowired
    private ThreadPoolTaskExecutor threadPoolTaskExecutor;

    SimpleDateFormat format = new SimpleDateFormat("yyyyMMdd HH:mm:ss");

    JsonUtils jsonUtils = new JsonUtils();

    @RequestMapping(value = WebGuiConstant.TERMINAL_GET_SDA_REQUEST, method = {RequestMethod.POST})
    public @ResponseBody
    ListSDAResponse listSda(@RequestBody final ListSDARequest request,
                            @PathVariable("apkVersion") String apkVersion) throws Exception {

        String username = request.getUsername();
        String imei = request.getImei();
        String sessionKey = request.getSessionKey();
        String areaCategory = request.getAreaCategory();
        String page = request.getPage().toString();
        String startLookupDate = request.getStartLookupDate();
        String endLookupDate = request.getEndLookupDate();
        String getCountData = request.getGetCountData().toString();
        final ListSDAResponse responseCode = new ListSDAResponse();
        responseCode.setResponseCode(WebGuiConstant.RC_SUCCESS);
        try {
            log.info("listSda INCOMING MESSAGE : " + jsonUtils.toJson(request));
            String validation = wsValidationService.wsValidation(username, imei, sessionKey, apkVersion);
            if (!validation.equals(WebGuiConstant.RC_SUCCESS)) {
                responseCode.setResponseCode(validation);
                log.error("Validation Failed for username : " + username + " ,imei : " + imei + " ,sessionKey : "
                        + sessionKey);
                String label = getMessage("webservice.rc.label." + responseCode.getResponseCode());
                responseCode.setResponseMessage(label);
                return responseCode;
            } else {
                log.debug("Validation success, get area data");
                AreaDistrict areaDistrictByUser = syncAreaService.getAreaDistrictByUsername(username);
                User user = userService.findUserByUsername(username);
                List<String> usernameList = new ArrayList<>();
                List<User> userList = userService.loadUserByLocationId(user.getOfficeCode());
                for (User userMap : userList) {
                    String usernameMap = userMap.getUsername();
                    usernameList.add(usernameMap);
                }
                if (areaDistrictByUser != null) {
                    if (areaCategory.toUpperCase().equals(WebGuiConstant.SDA_KECAMATAN)) {
                        log.debug("Request for Data Sub District");
                        Timer timer = new Timer();
                        timer.start(
                                "[" + WebGuiConstant.TERMINAL_GET_SDA_REQUEST + "] [areaService.getAreaSubDistrict]");
                        Page<AreaSubDistrictDetails> areaDistrictListPojo = sdaService.getAreaSubDistrict(page,
                                getCountData, startLookupDate, endLookupDate, usernameList);
                        timer.stop();
                        for (AreaSubDistrictDetails areaSubDistrictDetails : areaDistrictListPojo) {
                            KkHistoryResponse history = new KkHistoryResponse();
                            history.setCountKk(Integer.parseInt(areaSubDistrictDetails.getCountKKMiskin()));
                            history.setSdaId(areaSubDistrictDetails.getAreaDetailId() + "");
                            history.setAreaId(areaSubDistrictDetails.getAreaSubDistrict().getAreaId());
                            history.setCreatedBy(areaSubDistrictDetails.getCreatedBy());
                            if (areaSubDistrictDetails.getCreatedDate() != null)
                                history.setCreatedAt(format.format(areaSubDistrictDetails.getCreatedDate()));
                            history.setPejabat(areaSubDistrictDetails.getPejabat());
                            history.setJabatan(areaSubDistrictDetails.getJabatan());
                            history.setPhoneNumber(areaSubDistrictDetails.getPhoneNumber());
                            if (areaSubDistrictDetails.getUpdatedDate() != null)
                                history.setUpdatedAt(format.format(areaSubDistrictDetails.getUpdatedDate()));
                            responseCode.getKkMiskinHistory().add(history);
                        }
                        log.debug("Get data sub district success");
                        responseCode.setCurrentTotal(String.valueOf(areaDistrictListPojo.getContent().size()));
                        responseCode.setGrandTotal(String.valueOf(areaDistrictListPojo.getTotalElements()));
                        responseCode.setTotalPage(String.valueOf(areaDistrictListPojo.getTotalPages()));
                    } else if (areaCategory.toUpperCase().equals(WebGuiConstant.SDA_KELURAHAN)) {
                        log.debug("Request for Data Kelurahan");
                        Timer timer = new Timer();
                        timer.start("[" + WebGuiConstant.TERMINAL_GET_SDA_REQUEST + "] [areaService.getAreaKelurahan]");
                        Page<AreaKelurahanDetails> areaKelurahanListPojo = sdaService.getAreaKelurahan(page,
                                getCountData, startLookupDate, endLookupDate, usernameList);
                        timer.stop();
                        for (AreaKelurahanDetails areaKelurahanDetails : areaKelurahanListPojo) {
                            KkHistoryResponse history = new KkHistoryResponse();
                            history.setCountKk(Integer.parseInt(areaKelurahanDetails.getCountKKMiskin()));
                            history.setSdaId(areaKelurahanDetails.getAreaDetailId() + "");
                            history.setAreaId(areaKelurahanDetails.getAreaKelurahan().getAreaId());
                            history.setCreatedBy(areaKelurahanDetails.getCreatedBy());
                            if (areaKelurahanDetails.getCreatedDate() != null)
                                history.setCreatedAt(format.format(areaKelurahanDetails.getCreatedDate()));
                            history.setPejabat(areaKelurahanDetails.getPejabat());
                            history.setJabatan(areaKelurahanDetails.getJabatan());
                            history.setPhoneNumber(areaKelurahanDetails.getPhoneNumber());
                            if (areaKelurahanDetails.getUpdatedDate() != null)
                                history.setUpdatedAt(format.format(areaKelurahanDetails.getUpdatedDate()));
                            responseCode.getKkMiskinHistory().add(history);
                        }
                        if (areaKelurahanListPojo.getContent().size() > 0 && request.getPage() == null)
                            request.setPage(1);

                        Terminal terminal = terminalService.loadTerminalByImei(request.getImei());
                        terminal.setSdaProgress(PercentageSynchronizer.processSyncPercent(request.getPage().toString(),
                                String.valueOf(areaKelurahanListPojo.getTotalPages()), terminal.getSdaProgress()));
                        terminalService.updateTerminal(terminal);
                        log.debug("Get data Kelurahan success");
                        responseCode.setCurrentTotal(String.valueOf(areaKelurahanListPojo.getContent().size()));
                        responseCode.setGrandTotal(String.valueOf(areaKelurahanListPojo.getTotalElements()));
                        responseCode.setTotalPage(String.valueOf(areaKelurahanListPojo.getTotalPages()));
                    } else {
                        log.error("Invalid Area Category");
                        responseCode.setResponseCode(WebGuiConstant.RC_INVALID_SDA_CATEGORY);
                    }
                } else {
                    responseCode.setResponseCode(WebGuiConstant.RC_INVALID_SDA_CATEGORY);
                }
            }
            String label = getMessage("webservice.rc.label." + responseCode.getResponseCode());
            responseCode.setResponseMessage(label);
        } catch (Exception e) {
            log.error("listSda error: " + e.getMessage());
            responseCode.setResponseCode(WebGuiConstant.RC_GENERAL_ERROR);
            responseCode.setResponseMessage("listSda error: " + e.getMessage());
        } finally {
            try {
                log.debug("Try to create Terminal Activity");
                threadPoolTaskExecutor.execute(new Runnable() {
                    @Override
                    public void run() {
                        Terminal terminal = terminalService.loadTerminalByImei(request.getImei());
                        TerminalActivity terminalActivity = new TerminalActivity();
                        // Logging to terminal activity
                        terminalActivity.setTerminalActivityId(TransactionIdGenerator.generateTransactionId());
                        terminalActivity.setActivityType(TerminalActivity.ACTIVITY_SYNC_SDA);
                        terminalActivity.setCreatedDate(new Date());
                        terminalActivity.setReffNo(request.getRetrievalReferenceNumber().trim());
                        terminalActivity.setSessionKey(request.getSessionKey());
                        terminalActivity.setTerminal(terminal);
                        terminalActivity.setUsername(request.getUsername().trim());
                        List<MessageLogs> messageLogs = new ArrayList<>();
                        terminalActivityService.saveTerminalActivityAndMessageLogs(terminalActivity, messageLogs);
                    }
                });
                log.info("listSda RESPONSE MESSAGE : " + jsonUtils.toJson(responseCode));
            } catch (Exception e) {
                log.error("listSda saveTerminalActivityAndMessageLogs error: " + e.getMessage());
            }
        }
        return responseCode;
    }

}