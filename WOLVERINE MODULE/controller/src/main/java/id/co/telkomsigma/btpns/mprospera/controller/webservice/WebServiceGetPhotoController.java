package id.co.telkomsigma.btpns.mprospera.controller.webservice;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import id.co.telkomsigma.btpns.mprospera.constant.WebGuiConstant;
import id.co.telkomsigma.btpns.mprospera.controller.GenericController;
import id.co.telkomsigma.btpns.mprospera.model.messageLogs.MessageLogs;
import id.co.telkomsigma.btpns.mprospera.model.sw.SurveyWawancara;
import id.co.telkomsigma.btpns.mprospera.model.sw.SwIdPhoto;
import id.co.telkomsigma.btpns.mprospera.model.sw.SwSurveyPhoto;
import id.co.telkomsigma.btpns.mprospera.model.terminal.Terminal;
import id.co.telkomsigma.btpns.mprospera.model.terminal.TerminalActivity;
import id.co.telkomsigma.btpns.mprospera.request.PhotoRequest;
import id.co.telkomsigma.btpns.mprospera.response.PhotoResponse;
import id.co.telkomsigma.btpns.mprospera.service.CustomerService;
import id.co.telkomsigma.btpns.mprospera.service.LoanService;
import id.co.telkomsigma.btpns.mprospera.service.SWService;
import id.co.telkomsigma.btpns.mprospera.service.TerminalActivityService;
import id.co.telkomsigma.btpns.mprospera.service.TerminalService;
import id.co.telkomsigma.btpns.mprospera.service.WSValidationService;
import id.co.telkomsigma.btpns.mprospera.util.JsonUtils;
import id.co.telkomsigma.btpns.mprospera.util.TransactionIdGenerator;
import org.apache.tomcat.util.codec.binary.Base64;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller("webServiceGetPhotoController")
public class WebServiceGetPhotoController extends GenericController {

    @Autowired
    private WSValidationService wsValidationService;

    @Autowired
    private ThreadPoolTaskExecutor threadPoolTaskExecutor;

    @Autowired
    private TerminalActivityService terminalActivityService;

    @Autowired
    private TerminalService terminalService;

    @Autowired
    private LoanService loanService;

    @Autowired
    private CustomerService customerService;

    @Autowired
    private SWService swService;

    JsonUtils jsonUtils = new JsonUtils();

    @RequestMapping(value = WebGuiConstant.TERMINAL_GET_ID_PHOTO_LINK_REQUEST, method = {RequestMethod.POST})
    public void getIdPhoto(final HttpServletRequest request, final HttpServletResponse response,
                           @PathVariable("apkVersion") String apkVersion, @PathVariable("id") String id,
                           @RequestHeader(value = "transmissionDateAndTime", required = true) String transmissionDateAndTime,
                           @RequestHeader(value = "retrievalReferenceNumber", required = true) final String retrievalReferenceNumber,
                           @RequestHeader(value = "username", required = true) final String username,
                           @RequestHeader(value = "sessionKey", required = true) final String sessionKey,
                           @RequestHeader(value = "imei", required = true) final String imei) {

        final PhotoRequest requestMapping = new PhotoRequest();
        final PhotoResponse responseCode = new PhotoResponse();
        SwIdPhoto photo = null;
        try {
            log.info("getIdPhoto INCOMING MESSAGE");
            requestMapping.setTransmissionDateAndTime(transmissionDateAndTime);
            requestMapping.setRetrievalReferenceNumber(retrievalReferenceNumber);
            requestMapping.setUsername(username);
            requestMapping.setSessionKey(sessionKey);
            requestMapping.setImei(imei);
            responseCode.setResponseCode(WebGuiConstant.RC_SUCCESS);
            log.debug("Validating Request");
            String validation = wsValidationService.wsValidation(username, imei, sessionKey, apkVersion);
            if (!validation.equals(WebGuiConstant.RC_SUCCESS)) {
                responseCode.setResponseCode(validation);
                String label = getMessage("webservice.rc.label." + responseCode.getResponseCode());
                responseCode.setResponseMessage(label);
                log.error("Validation Failed for username : " + username + " ,imei : " + imei + " ,sessionKey : " + sessionKey);
            } else {
                String label = getMessage("webservice.rc.label." + responseCode.getResponseCode());
                responseCode.setResponseMessage(label);
                log.debug("Get ID Card Photo");
                SurveyWawancara swLocal = swService.findSwByLocalId(id);
                if (swLocal != null) {
                    photo = swService.getSwIdPhoto(swLocal.getSwId().toString());
                    if (photo != null) {
                        responseCode.setPhoto(photo.getIdPhoto());
                    } else {
                        responseCode.setResponseCode(WebGuiConstant.RC_ID_PHOTO_NULL);
                        String labelError = getMessage("webservice.rc.label." + responseCode.getResponseCode());
                        responseCode.setResponseMessage(labelError);
                    }
                } else {
                    responseCode.setResponseCode(WebGuiConstant.RC_ID_PHOTO_NULL);
                    String labelError = getMessage("webservice.rc.label." + responseCode.getResponseCode());
                    responseCode.setResponseMessage(labelError);
                }
            }
        } catch (Exception e) {
            log.error("getIdPhoto error: " + e.getMessage());
            responseCode.setResponseCode(WebGuiConstant.RC_GENERAL_ERROR);
            responseCode.setResponseMessage("getIdPhoto error: " + e.getMessage());
        } finally {
            try {
                log.debug("Try to create Terminal Activity");
                threadPoolTaskExecutor.execute(new Runnable() {
                    @Override
                    public void run() {
                        Terminal terminal = terminalService.loadTerminalByImei(requestMapping.getImei());
                        TerminalActivity terminalActivity = new TerminalActivity();
                        // Logging to terminal activity
                        terminalActivity.setTerminalActivityId(TransactionIdGenerator.generateTransactionId());
                        terminalActivity.setActivityType(TerminalActivity.ACTIVITY_GET_getIdPhoto);
                        terminalActivity.setCreatedDate(new Date());
                        terminalActivity.setReffNo(requestMapping.getRetrievalReferenceNumber().trim());
                        terminalActivity.setSessionKey(requestMapping.getSessionKey());
                        terminalActivity.setTerminal(terminal);
                        terminalActivity.setUsername(requestMapping.getUsername().trim());
                        List<MessageLogs> messageLogs = new ArrayList<>();
                        terminalActivityService.saveTerminalActivityAndMessageLogs(terminalActivity, messageLogs);
                    }
                });
                log.debug("Finishing create Terminal Activity");
                log.info("getIdPhoto RESPONSE MESSAGE : " + jsonUtils.toJson(responseCode));
            } catch (Exception e) {
                log.error("getIdPhoto saveTerminalActivityAndMessageLogs error: " + e.getMessage());
            }
            try {
                if (photo != null) {
                    response.setContentType("image/jpg;charset=utf-8");
                    response.getOutputStream().write(Base64.decodeBase64(responseCode.getPhoto()));
                    response.getOutputStream().flush();
                } else
                    response.getWriter().write((new JsonUtils().toJson(responseCode)));
            } catch (Exception e) {
                System.out.println(e.getMessage());
            }
        }
    }

    @RequestMapping(value = WebGuiConstant.TERMINAL_BUSINESS_PLACE_PHOTO_LINK_REQUEST, method = {RequestMethod.POST})
    public void getBusinessPlacePhoto(final HttpServletRequest request, final HttpServletResponse response,
                                      @PathVariable("apkVersion") String apkVersion, @PathVariable("id") String id,
                                      @RequestHeader(value = "transmissionDateAndTime", required = true) String transmissionDateAndTime,
                                      @RequestHeader(value = "retrievalReferenceNumber", required = true) final String retrievalReferenceNumber,
                                      @RequestHeader(value = "username", required = true) final String username,
                                      @RequestHeader(value = "sessionKey", required = true) final String sessionKey,
                                      @RequestHeader(value = "imei", required = true) final String imei) {

        final PhotoRequest requestMapping = new PhotoRequest();
        final PhotoResponse responseCode = new PhotoResponse();
        responseCode.setResponseCode(WebGuiConstant.RC_SUCCESS);
        SwSurveyPhoto photo = null;
        log.info("getBusinessPlacePhoto INCOMING MESSAGE");
        try {
            requestMapping.setTransmissionDateAndTime(transmissionDateAndTime);
            requestMapping.setRetrievalReferenceNumber(retrievalReferenceNumber);
            requestMapping.setUsername(username);
            requestMapping.setSessionKey(sessionKey);
            requestMapping.setImei(imei);
            log.debug("Validating Request");
            String validation = wsValidationService.wsValidation(username, imei, sessionKey, apkVersion);
            if (!validation.equals(WebGuiConstant.RC_SUCCESS)) {
                responseCode.setResponseCode(validation);
                String label = getMessage("webservice.rc.label." + responseCode.getResponseCode());
                responseCode.setResponseMessage(label);
                log.error("Validation Failed for username : " + username + " ,imei : " + imei + " ,sessionKey : " + sessionKey);
            } else {
                String label = getMessage("webservice.rc.label." + responseCode.getResponseCode());
                responseCode.setResponseMessage(label);
                log.debug("Get Business Place Photo");
                SurveyWawancara swLocal = swService.findSwByLocalId(id);
                if (swLocal != null) {
                    photo = swService.getSwSurveyPhoto(swLocal.getSwId().toString());
                    if (photo != null) {
                        responseCode.setPhoto(photo.getSurveyPhoto());
                    } else {
                        responseCode.setResponseCode(WebGuiConstant.RC_SURVEY_PHOTO_NULL);
                        String labelError = getMessage("webservice.rc.label." + responseCode.getResponseCode());
                        responseCode.setResponseMessage(labelError);
                    }
                } else {
                    responseCode.setResponseCode(WebGuiConstant.RC_SURVEY_PHOTO_NULL);
                    String labelError = getMessage("webservice.rc.label." + responseCode.getResponseCode());
                    responseCode.setResponseMessage(labelError);
                }
            }
        } catch (Exception e) {
            log.error("getBusinessPlacePhoto error: " + e.getMessage());
            responseCode.setResponseCode(WebGuiConstant.RC_GENERAL_ERROR);
            responseCode.setResponseMessage("getBusinessPlacePhoto error: " + e.getMessage());
        } finally {
            try {
                log.debug("Try to create Terminal Activity");
                threadPoolTaskExecutor.execute(new Runnable() {
                    @Override
                    public void run() {
                        Terminal terminal = terminalService.loadTerminalByImei(requestMapping.getImei());
                        TerminalActivity terminalActivity = new TerminalActivity();
                        // Logging to terminal activity
                        terminalActivity.setTerminalActivityId(TransactionIdGenerator.generateTransactionId());
                        terminalActivity.setActivityType(TerminalActivity.ACTIVITY_GET_getBusinessPlacePhoto);
                        terminalActivity.setCreatedDate(new Date());
                        terminalActivity.setReffNo(requestMapping.getRetrievalReferenceNumber().trim());
                        terminalActivity.setSessionKey(requestMapping.getSessionKey());
                        terminalActivity.setTerminal(terminal);
                        terminalActivity.setUsername(requestMapping.getUsername().trim());
                        List<MessageLogs> messageLogs = new ArrayList<>();
                        terminalActivityService.saveTerminalActivityAndMessageLogs(terminalActivity, messageLogs);
                    }
                });
                log.debug("Finishing create Terminal Activity");
                log.info("getBusinessPlacePhoto RESPONSE MESSAGE : " + jsonUtils.toJson(responseCode));
            } catch (Exception e) {
                log.error("getBusinessPlacePhoto saveTerminalActivityAndMessageLogs error: " + e.getMessage());
            }
            try {
                if (photo != null) {
                    response.setContentType("image/jpg;charset=utf-8");
                    response.getOutputStream().write(Base64.decodeBase64(responseCode.getPhoto()));
                    response.getOutputStream().flush();
                } else
                    response.getWriter().write((new JsonUtils().toJson(responseCode)));
            } catch (Exception e) {
                System.out.println(e.getMessage());
            }
        }
    }

}