package id.co.telkomsigma.btpns.mprospera.controller.configuration;

import com.github.isrsal.logging.LoggingFilter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.LocaleResolver;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurationSupport;
import org.springframework.web.servlet.i18n.LocaleChangeInterceptor;
import org.springframework.web.servlet.i18n.SessionLocaleResolver;

import javax.servlet.Filter;
import java.util.Locale;

/**
 * Created by daniel on 3/26/15.
 */
@Configuration
public class WebMvcConfig extends WebMvcConfigurationSupport {

    @Value("${default.locale.lang}")
    private String localeLanguage;

    @Value("${default.locale.country}")
    private String localeCountry;

    @Value("${default.timezone.lang}")
    private String timezoneLanguage;

    @Value("${default.timezone.country}")
    private String timezoneCountry;

    private static Locale language;
    private static Locale timezone;

    @Autowired
    ApplicationContext applicationContext;

    @Bean(name = "language")
    public Locale getLanguage() {
        if (language == null) {
            language = new Locale(localeLanguage, localeCountry);
        }
        return language;
    }

    @Bean(name = "timezone")
    public Locale getTimezone() {
        if (timezone == null) {
            timezone = new Locale(timezoneLanguage, timezoneCountry);
        }
        return timezone;
    }

    @Bean(name = "localeResolver")
    public LocaleResolver localeResolver() {
        SessionLocaleResolver slr = new SessionLocaleResolver();
        slr.setDefaultLocale(getLanguage());
        return slr;
    }

    @Bean
    public LocaleChangeInterceptor localeChangeInterceptor() {
        LocaleChangeInterceptor lci = new LocaleChangeInterceptor();
        lci.setParamName("lang");
        return lci;
    }

    @Override
    public void addResourceHandlers(ResourceHandlerRegistry registry) {
        registry.addResourceHandler("/css/**").setCachePeriod(Integer.MAX_VALUE)
                .addResourceLocations("classpath:/static/css/");
        registry.addResourceHandler("/fonts/**").setCachePeriod(Integer.MAX_VALUE)
                .addResourceLocations("classpath:/static/fonts/");
        registry.addResourceHandler("/img/**").setCachePeriod(Integer.MAX_VALUE)
                .addResourceLocations("classpath:/static/img/");
        registry.addResourceHandler("/js/**").setCachePeriod(Integer.MAX_VALUE)
                .addResourceLocations("classpath:/static/js/");
    }

    @Bean
    public Filter logFilter() {
        return new LoggingFilter();
    }

}