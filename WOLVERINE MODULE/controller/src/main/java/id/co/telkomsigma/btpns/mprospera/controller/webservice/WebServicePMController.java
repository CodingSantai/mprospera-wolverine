package id.co.telkomsigma.btpns.mprospera.controller.webservice;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import id.co.telkomsigma.btpns.mprospera.constant.WebGuiConstant;
import id.co.telkomsigma.btpns.mprospera.controller.GenericController;
import id.co.telkomsigma.btpns.mprospera.model.messageLogs.MessageLogs;
import id.co.telkomsigma.btpns.mprospera.model.pm.ProjectionMeeting;
import id.co.telkomsigma.btpns.mprospera.model.security.AuditLog;
import id.co.telkomsigma.btpns.mprospera.model.sw.SurveyWawancara;
import id.co.telkomsigma.btpns.mprospera.model.terminal.Terminal;
import id.co.telkomsigma.btpns.mprospera.model.terminal.TerminalActivity;
import id.co.telkomsigma.btpns.mprospera.model.user.User;
import id.co.telkomsigma.btpns.mprospera.request.PMRequest;
import id.co.telkomsigma.btpns.mprospera.request.SWProfileReq;
import id.co.telkomsigma.btpns.mprospera.request.SWRequest;
import id.co.telkomsigma.btpns.mprospera.response.ListPMResponse;
import id.co.telkomsigma.btpns.mprospera.response.ListSWResponse;
import id.co.telkomsigma.btpns.mprospera.response.PMResponse;
import id.co.telkomsigma.btpns.mprospera.service.*;
import id.co.telkomsigma.btpns.mprospera.util.JsonUtils;
import id.co.telkomsigma.btpns.mprospera.util.PercentageSynchronizer;
import id.co.telkomsigma.btpns.mprospera.util.TransactionIdGenerator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

@SuppressWarnings("unchecked")
@Controller
public class WebServicePMController extends GenericController {

    @Autowired
    private WSValidationService wsValidationService;

    @Autowired
    private AuditLogService auditLogService;

    @Autowired
    private PmService pmService;

    @Autowired
    private SWService swService;

    @Autowired
    private TerminalActivityService terminalActivityService;

    @Autowired
    private ThreadPoolTaskExecutor threadPoolTaskExecutor;

    @Autowired
    private TerminalService terminalService;

    @Autowired
    private UserService userService;

    SimpleDateFormat formatter = new SimpleDateFormat("yyyyMMdd hh:mm:ss");
    SimpleDateFormat formatDate = new SimpleDateFormat("yyyyMMdd");

    JsonUtils jsonUtils = new JsonUtils();

    @RequestMapping(value = WebGuiConstant.PM_SUBMIT_DATA_REQUEST, method = {RequestMethod.POST})
    public @ResponseBody
    PMResponse doAdd(@RequestBody String requestString,
                     @PathVariable("apkVersion") String apkVersion) throws Exception {

        Object obj = new JsonUtils().fromJson(requestString, PMRequest.class);
        final PMRequest request = (PMRequest) obj;
        final PMResponse responseCode = new PMResponse();
        responseCode.setResponseCode(WebGuiConstant.RC_SUCCESS);
        String pmId = "";
        try {
            log.info("addPM INCOMING MESSAGE : " + jsonUtils.toJson(request));
            // validation
            String validation = wsValidationService.wsValidation(request.getUsername(), request.getImei(),
                    request.getSessionKey(), apkVersion);
            if (!validation.equals(WebGuiConstant.RC_SUCCESS)) {
                responseCode.setResponseCode(validation);
                String label = getMessage("webservice.rc.label." + responseCode.getResponseCode());
                responseCode.setResponseMessage(label);
                log.error("Validation Failed for username : " + request.getUsername() + " ,imei : " + request.getImei()
                        + " ,sessionKey : " + request.getSessionKey());
            } else {
                log.debug("Try Insert to PM TABLE");
                // insert ke tabel PM
                ProjectionMeeting pm = new ProjectionMeeting();
                pm.setAreaId(request.getAreaId());
                pm.setCreatedBy(request.getUsername());
                pm.setCreatedDate(new Date());
                pm.setLatitude(request.getLatitude());
                pm.setLongitude(request.getLongitude());
                pm.setPmLocationName(request.getPmLocationName());
                pm.setPmNumber(request.getPmParticipant());
                pm.setPmOwner(request.getPmOwner());
                pm.setSwCandidate(request.getSwCandidate());
                if (request.getPmDate() != null) {
                    if (!request.getPmDate().equals(""))
                        pm.setPmDate(formatter.parse(request.getPmDate()));
                }
                pm.setStatus(WebGuiConstant.STATUS_SUBMIT);
                if ("delete".equals(request.getAction())) {
                    ProjectionMeeting pmFromDb = pmService.findById(request.getPmId());
                    pmFromDb.setIsDeleted(true);
                    pm = pmFromDb;
                }
                if ("delete".equals(request.getAction()) || "update".equals(request.getAction())) {
                    if (request.getPmId() != null) {
                        if (!request.getPmId().equals("")) {
                            pm.setPmId(Long.parseLong(request.getPmId()));
                            ProjectionMeeting pmFromDb = pmService.findById(request.getPmId());
                            pm.setPmOwnerPhoneNumber(pmFromDb.getPmOwnerPhoneNumber());
                            pm.setPmPlanNumber(pmFromDb.getPmPlanNumber());
                            pm.setTime(pmFromDb.getTime());
                            pm.setMmId(pmFromDb.getMmId());
                            pm.setUpdatedBy(request.getUsername());
                            pm.setUpdatedDate(new Date());
                        } else {
                            responseCode.setResponseCode(WebGuiConstant.RC_UNKNOWN_PM_ID);
                        }
                    } else {
                        responseCode.setResponseCode(WebGuiConstant.RC_UNKNOWN_PM_ID);
                    }
                }
                if (("delete".equals(request.getAction()) || "update".equals(request.getAction()))
                        && !pmService.isValidPm(request.getPmId()))
                    responseCode.setResponseCode(WebGuiConstant.RC_UNKNOWN_PM_ID);
                else {
                    if (!terminalService.isDuplicate(request.getRetrievalReferenceNumber(), "addPM")) {
                        pmService.save(pm);
                        pmId = pm.getPmId().toString();
                        List<ListSWResponse> swResponse = new ArrayList();
                        if (request.getSwPlanList() != null) {
                            for (SWRequest swReq : request.getSwPlanList()) {
                                SurveyWawancara swPlan = new SurveyWawancara();
                                swPlan.setAreaId(request.getAreaId());
                                swPlan.setCreatedBy(request.getUsername());
                                swPlan.setCreatedDate(new Date());
                                SWProfileReq swProfileReq = swReq.getProfile();
                                swPlan.setCustomerName(swProfileReq.getCustomerName());
                                User userPS = userService.findUserByUsername(request.getUsername());
                                swPlan.setWismaId(userPS.getOfficeCode());
                                swPlan.setPhoneNumber(swProfileReq.getPhoneNumber());
                                swPlan.setSwLocation(swReq.getSwLocation());
                                swPlan.setSurveyDate(formatDate.parse(swReq.getSurveyDate()));
                                swPlan.setTime(swReq.getDayLight());
                                swPlan.setStatus(WebGuiConstant.STATUS_PLAN);
                                swPlan.setPmId(pm);
                                swPlan.setLocalId(swReq.getSwId());
                                String localId = swReq.getSwId();
                                if ("delete".equals(swReq.getAction()))
                                    swPlan.setIsDeleted(true);
                                if ("delete".equals(swReq.getAction()) || "update".equals(swReq.getAction())) {
                                    if (null != swReq.getSwId()) {
                                        if (!swReq.getSwId().equals("")) {
                                            SurveyWawancara swLocal = swService.findSwByLocalId(swReq.getSwId());
                                            if (swLocal == null) {
                                                responseCode.setResponseCode(WebGuiConstant.RC_UNKNOWN_SW_ID);
                                                String label = getMessage(
                                                        "webservice.rc.label." + responseCode.getResponseCode());
                                                responseCode.setResponseMessage(label);
                                                return responseCode;
                                            }
                                            if (!swService.isValidSw(swLocal.getSwId().toString())) {
                                                responseCode.setResponseCode(WebGuiConstant.RC_UNKNOWN_SW_ID);
                                            } else {
                                                swPlan.setStatus(swLocal.getStatus());
                                                swPlan.setSwId(swLocal.getSwId());
                                                swPlan.setLocalId(localId);
                                            }
                                        } else {
                                            responseCode.setResponseCode(WebGuiConstant.RC_UNKNOWN_SW_ID);
                                        }
                                    } else
                                        responseCode.setResponseCode(WebGuiConstant.RC_UNKNOWN_SW_ID);
                                }
                                swService.save(swPlan);
                                ListSWResponse sw = new ListSWResponse();
                                sw.setSwId(swPlan.getLocalId());
                                sw.setStatus(swPlan.getStatus());
                                swResponse.add(sw);
                            }
                            responseCode.setSwPlanList(swResponse);
                        }
                    } else {
                        Timer timer2 = new Timer();
                        timer2.start("[" + WebGuiConstant.PM_SUBMIT_DATA_REQUEST + "] [pmService.getPmByRrn]");
                        ProjectionMeeting pmExist = pmService.getPmByRrn(request.getRetrievalReferenceNumber());
                        timer2.stop();
                        if (pmExist != null)
                            pmId = pmExist.getPmId().toString();
                    }
                }
            }
            String label = getMessage("webservice.rc.label." + responseCode.getResponseCode());
            responseCode.setResponseMessage(label);
            responseCode.setPmId(pmId);
            responseCode.setLocalId(request.getLocalId());
        } catch (Exception e) {
            log.error("addPM error: " + e.getMessage());
            responseCode.setResponseCode(WebGuiConstant.RC_GENERAL_ERROR);
            responseCode.setResponseMessage("addPM error: " + e.getMessage());
        } finally {
            // insert MessageLogs
            try {
                threadPoolTaskExecutor.execute(new Runnable() {
                    @Override
                    public void run() {
                        log.debug("Trying to CREATE Audit Log...");
                        // insert audit log
                        AuditLog auditLog = new AuditLog();
                        auditLog.setActivityType(AuditLog.MODIF_PM);
                        auditLog.setCreatedBy(request.getUsername());
                        auditLog.setCreatedDate(new Date());
                        auditLog.setDescription(request.toString() + " | " + responseCode.toString());
                        auditLog.setReffNo(request.getRetrievalReferenceNumber());
                        log.debug(auditLog);
                        auditLogService.insertAuditLog(auditLog);
                        log.debug("AUDIT LOG LOGGING SUCCESS...");
                        log.debug("Updating AUDIT LOG");
                        log.debug("Trying to CREATE Terminal Activity...");
                        Terminal terminal = terminalService.loadTerminalByImei(request.getImei());
                        TerminalActivity terminalActivity = new TerminalActivity();
                        // Logging to terminal activity
                        terminalActivity.setTerminalActivityId(TransactionIdGenerator.generateTransactionId());
                        terminalActivity.setActivityType(TerminalActivity.ACTIVITY_MODY_PM);
                        terminalActivity.setCreatedDate(new Date());
                        terminalActivity.setReffNo(request.getRetrievalReferenceNumber().trim());
                        terminalActivity.setSessionKey(request.getSessionKey());
                        terminalActivity.setTerminal(terminal);
                        terminalActivity.setUsername(request.getUsername().trim());
                        List<MessageLogs> messageLogs = new ArrayList<>();
                        terminalActivityService.insertRrn("addPM", request.getRetrievalReferenceNumber().trim());
                        terminalActivityService.saveTerminalActivityAndMessageLogs(terminalActivity, messageLogs);
                        log.debug("TERMINAL ACTIVITY and MESSAGE LOGS LOGGING SUCCESS...");
                        log.debug("Updating Terminal Activity");
                    }
                });
                log.info("addPM RESPONSE MESSAGE : " + jsonUtils.toJson(responseCode));
            } catch (Exception e) {
                log.error("addPM ERROR TerminalAcivity, method doAdd, Class WebServicePMController" + e.getMessage());
            }
        }
        return responseCode;
    }

    @RequestMapping(value = WebGuiConstant.TERMINAL_GET_PM_REQUEST, method = {RequestMethod.POST})
    public @ResponseBody
    PMResponse doList(@RequestBody String requestString,
                      @PathVariable("apkVersion") String apkVersion) throws Exception {

        Object obj = new JsonUtils().fromJson(requestString, PMRequest.class);
        final PMRequest request = (PMRequest) obj;
        final PMResponse responseCode = new PMResponse();
        responseCode.setResponseCode(WebGuiConstant.RC_SUCCESS);
        try {
            log.info("listPM INCOMING MESSAGE : " + jsonUtils.toJson(request));
            // validation
            String validation = wsValidationService.wsValidation(request.getUsername(), request.getImei(),
                    request.getSessionKey(), apkVersion);
            if (!validation.equals(WebGuiConstant.RC_SUCCESS)) {
                responseCode.setResponseCode(validation);
                String label = getMessage("webservice.rc.label." + responseCode.getResponseCode());
                responseCode.setResponseMessage(label);
                log.error("Validation Failed for username : " + request.getUsername() + " ,imei : " + request.getImei()
                        + " ,sessionKey : " + request.getSessionKey());
                return responseCode;
            } else {
                try {
                    Timer timer = new Timer();
                    timer.start("[" + WebGuiConstant.TERMINAL_GET_PM_REQUEST + "] [pmService.getPmByCreatedDate]");
                    final Page<ProjectionMeeting> projectionMeetingListPojo = pmService.getPmByCreatedDateAndUser(
                            request.getPage().toString(), request.getGetCountData().toString(),
                            request.getStartLookupDate(), request.getEndLookupDate(), request.getUsername());
                    timer.stop();
                    final List<ListPMResponse> pmResponses = new ArrayList<ListPMResponse>();
                    for (final ProjectionMeeting pm : projectionMeetingListPojo) {
                        // TODO Auto-generated method stub
                        ListPMResponse pmResponse = new ListPMResponse();
                        pmResponse.setPmId(pm.getPmId().toString());
                        if (pm.getMmId() != null)
                            pmResponse.setMmId(pm.getMmId().getMmId().toString());
                        else
                            pmResponse.setMmId("");
                        pmResponse.setCreatedBy(pm.getCreatedBy());
                        if(pmResponse.getUpdatedDate() != null) {
                           pmResponse.setUpdatedDate(formatter.format(pm.getUpdatedDate()));
                        }
                        pmResponse.setUpdatedBy(pm.getUpdatedBy());
                        pmResponse.setCreatedDate(formatter.format(pm.getCreatedDate()));
                        if (pm.getStatus().equals(WebGuiConstant.STATUS_PLAN))
                            pmResponse.setPmParticipant(pm.getPmPlanNumber());
                        else
                            pmResponse.setPmParticipant(pm.getPmNumber());
                        pmResponse.setPmLocationName(pm.getPmLocationName());
                        pmResponse.setPmOwner(pm.getPmOwner());
                        pmResponse.setSwCandidate(pm.getSwCandidate());
                        pmResponse.setTime(pm.getTime());
                        pmResponse.setPmOwnerPhoneNumber(pm.getPmOwnerPhoneNumber());
                        if (pm.getPmDate() != null)
                            pmResponse.setPmDate(formatter.format(pm.getPmDate()));
                        else
                            pmResponse.setPmDate("");
                        if (pm.getPmPlanDate() != null)
                            pmResponse.setPmPlanDate(formatter.format(pm.getPmPlanDate()));
                        else
                            pmResponse.setPmPlanDate("");
                        pmResponse.setStatus(pm.getStatus());
                        pmResponses.add(pmResponse);
                    }
                    Terminal terminal = terminalService.loadTerminalByImei(request.getImei());
                    terminal.setPmProgress(PercentageSynchronizer.processSyncPercent(request.getPage().toString(),
                            String.valueOf(projectionMeetingListPojo.getTotalPages()), terminal.getPmProgress()));
                    terminalService.updateTerminal(terminal);
                    final List<ProjectionMeeting> listDeletedPm = pmService.findIsDeletedPmList();
                    List<String> deletedPmList = new ArrayList<>();
                    for (ProjectionMeeting deletedPm : listDeletedPm) {
                        String id = deletedPm.getPmId().toString();
                        deletedPmList.add(id);
                    }
                    responseCode.setDeletedPmList(deletedPmList);
                    responseCode.setCurrentTotal(String.valueOf(projectionMeetingListPojo.getContent().size()));
                    responseCode.setGrandTotal(String.valueOf(projectionMeetingListPojo.getTotalElements()));
                    responseCode.setTotalPage(String.valueOf(projectionMeetingListPojo.getTotalPages()));
                    responseCode.setPmList(pmResponses);
                } catch (ParseException e) {
                    log.error("ERROR FETCHING WsValidationService : " + e.getMessage());
                }
            }
            String labelError = getMessage("webservice.rc.label." + responseCode.getResponseCode());
            responseCode.setResponseMessage(labelError);
        } catch (Exception e) {
            log.error("listPM error: " + e.getMessage());
            responseCode.setResponseCode(WebGuiConstant.RC_GENERAL_ERROR);
            responseCode.setResponseMessage("listPM error: " + e.getMessage());
        } finally {
            try {
                log.debug("Try to create Terminal Activity and Audit Log");
                // insert MessageLogs
                threadPoolTaskExecutor.execute(new Runnable() {
                    @Override
                    public void run() {
                        Terminal terminal = terminalService.loadTerminalByImei(request.getImei());
                        TerminalActivity terminalActivity = new TerminalActivity();
                        // Logging to terminal activity
                        terminalActivity.setTerminalActivityId(TransactionIdGenerator.generateTransactionId());
                        terminalActivity.setActivityType(TerminalActivity.ACTIVITY_SYNC_PM);
                        terminalActivity.setCreatedDate(new Date());
                        terminalActivity.setReffNo(request.getRetrievalReferenceNumber().trim());
                        terminalActivity.setSessionKey(request.getSessionKey());
                        terminalActivity.setTerminal(terminal);
                        terminalActivity.setUsername(request.getUsername().trim());
                        List<MessageLogs> messageLogs = new ArrayList<>();
                        terminalActivityService.saveTerminalActivityAndMessageLogs(terminalActivity, messageLogs);
                        log.debug("TERMINAL ACTIVITY LOGGING SUCCESS...");
                        log.debug("Updating Terminal Activity");
                    }
                });
                log.info("listPM RESPONSE MESSAGE : " + jsonUtils.toJson(responseCode));
            } catch (Exception e) {
                log.error("listPM saveTerminalActivityAndMessageLogs error: " + e.getMessage());
            }
        }
        return responseCode;
    }

}