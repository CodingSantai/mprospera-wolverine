package id.co.telkomsigma.btpns.mprospera.manager.impl;

import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.cache.annotation.Caching;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import id.co.telkomsigma.btpns.mprospera.dao.PmDao;
import id.co.telkomsigma.btpns.mprospera.manager.PmManager;
import id.co.telkomsigma.btpns.mprospera.model.mm.MiniMeeting;
import id.co.telkomsigma.btpns.mprospera.model.pm.ProjectionMeeting;

@SuppressWarnings("RedundantIfStatement")
@Service("pmManager")
public class PmManagerImpl implements PmManager {

    @Autowired
    private PmDao pmDao;

    @Override
    @Caching(evict = {@CacheEvict(value = "wln.pm.isValidPm", key = "#pm.pmId", beforeInvocation = true),
            @CacheEvict(value = "wln.pm.findById", key = "#pm.pmId", beforeInvocation = true),
            @CacheEvict(value = "wln.pm.getAllPm", allEntries = true, beforeInvocation = true),
            @CacheEvict(value = "wln.pm.getPm", allEntries = true, beforeInvocation = true),
            @CacheEvict(value = "wln.pm.getPmPageable", allEntries = true, beforeInvocation = true),
            @CacheEvict(value = "wln.pm.findPmByCreatedDate", allEntries = true, beforeInvocation = true),
            @CacheEvict(value = "wln.pm.findPmByCreatedDatePageable", allEntries = true, beforeInvocation = true),
            @CacheEvict(value = "wln.pm.getPmByUser", allEntries = true, beforeInvocation = true),
            @CacheEvict(value = "wln.pm.getPmByUserPageable", allEntries = true, beforeInvocation = true),
            @CacheEvict(value = "wln.pm.findPmByCreatedDateAndUser", allEntries = true, beforeInvocation = true),
            @CacheEvict(value = "wln.pm.findPmByCreatedDateAndUserPageable", allEntries = true, beforeInvocation = true),
            @CacheEvict(value = "wln.pm.getAllPmPageable", allEntries = true, beforeInvocation = true),
            @CacheEvict(value = "wln.pm.findByCreatedDate", allEntries = true, beforeInvocation = true),
            @CacheEvict(value = "wln.pm.findByCreatedDatePageable", allEntries = true, beforeInvocation = true),
            @CacheEvict(value = "wln.pm.findByRrn", key = "#pm.rrn", beforeInvocation = true),
            @CacheEvict(value = "wln.pm.findByMmId", key = "#pm.mmId", beforeInvocation = true),
            @CacheEvict(value = "wln.pm.findIsDeletedPmList", allEntries = true, beforeInvocation = true)})
    public void save(ProjectionMeeting pm) {
        pmDao.save(pm);
    }

    @Override
    @Cacheable(value = "wln.pm.isValidPm", unless = "#result == null")
    public Boolean isValidPm(String pmId) {
        Integer count = pmDao.countByPmId(Long.parseLong(pmId));
        if (count > 0)
            return true;
        else
            return false;
    }

    @Override
    @Cacheable(value = "wln.pm.getAllPm", unless = "#result == null")
    public Page<ProjectionMeeting> getAllPm(List<String> kelIdList) {
        // TODO Auto-generated method stub
        return pmDao.findByAreaIdIn(kelIdList, new PageRequest(0, Integer.MAX_VALUE));
    }

    @Override
    @Cacheable(value = "wln.pm.getPm", unless = "#result == null")
    public Page<ProjectionMeeting> getPm() {
        return pmDao.getAllPm(new PageRequest(0, Integer.MAX_VALUE));
    }

    @Override
    @Cacheable(value = "wln.pm.getPmPageable", unless = "#result == null")
    public Page<ProjectionMeeting> getPmPageable(PageRequest pageRequest) {
        return pmDao.getAllPm(pageRequest);
    }

    @Override
    @Cacheable(value = "wln.pm.findPmByCreatedDate", unless = "#result == null")
    public Page<ProjectionMeeting> findPmByCreatedDate(Date startDate, Date endDate) {
        return pmDao.findAllByCreatedDate(startDate, endDate, new PageRequest(0, Integer.MAX_VALUE));
    }

    @Override
    @Cacheable(value = "wln.pm.findPmByCreatedDatePageable", unless = "#result == null")
    public Page<ProjectionMeeting> findPmByCreatedDatePageable(Date startDate, Date endDate, PageRequest pageRequest) {
        return pmDao.findAllByCreatedDate(startDate, endDate, pageRequest);
    }

    @Override
    @Cacheable(value = "wln.pm.getPmByUser", unless = "#result == null")
    public Page<ProjectionMeeting> getPmByUser(List<String> userIdList) {
        return pmDao.getAllPmByUser(userIdList, new PageRequest(0, Integer.MAX_VALUE));
    }

    @Override
    @Cacheable(value = "wln.pm.getPmByUserPageable", unless = "#result == null")
    public Page<ProjectionMeeting> getPmByUserPageable(List<String> userIdList, PageRequest pageRequest) {
        return pmDao.getAllPmByUser(userIdList, pageRequest);
    }

    @Override
    @Cacheable(value = "wln.pm.findPmByCreatedDateAndUser", unless = "#result == null")
    public Page<ProjectionMeeting> findPmByCreatedDateAndUser(List<String> userIdList, Date startDate, Date endDate) {
        return pmDao.findAllByCreatedDateAndUser(userIdList, startDate, endDate, new PageRequest(0, Integer.MAX_VALUE));
    }

    @Override
    @Cacheable(value = "wln.pm.findPmByCreatedDateAndUserPageable", unless = "#result == null")
    public Page<ProjectionMeeting> findPmByCreatedDateAndUserPageable(List<String> userIdList, Date startDate, Date endDate, PageRequest pageRequest) {
        return pmDao.findAllByCreatedDateAndUser(userIdList, startDate, endDate, pageRequest);
    }

    @Override
    @Cacheable(value = "wln.pm.getAllPmPageable", unless = "#result == null")
    public Page<ProjectionMeeting> getAllPmPageable(List<String> kelIdList, PageRequest pageRequest) {
        // TODO Auto-generated method stub
        return pmDao.findByAreaIdIn(kelIdList, pageRequest);
    }

    @Override
    @Cacheable(value = "wln.pm.findByCreatedDate", unless = "#result == null")
    public Page<ProjectionMeeting> findByCreatedDate(List<String> kelIdList, Date startDate, Date endDate) {
        // TODO Auto-generated method stub
        return pmDao.findByCreatedDate(kelIdList, startDate, endDate, new PageRequest(0, Integer.MAX_VALUE));
    }

    @Override
    @Cacheable(value = "wln.pm.findByCreatedDatePageable", unless = "#result == null")
    public Page<ProjectionMeeting> findByCreatedDatePageable(List<String> kelIdList, Date startDate, Date endDate,
                                                             PageRequest pageRequest) {
        // TODO Auto-generated method stub
        return pmDao.findByCreatedDate(kelIdList, startDate, endDate, pageRequest);
    }

    @Override
    @CacheEvict(value = {"wln.pm.findById", "wln.pm.isValidPm", "wln.pm.getAllPm", "wln.pm.getPm",
            "wln.pm.getPmPageable", "wln.pm.findPmByCreatedDate", "wln.pm.findPmByCreatedDatePageable",
            "wln.pm.getPmByUser", "wln.pm.getPmByUserPageable", "wln.pm.findPmByCreatedDateAndUser",
            "wln.pm.findPmByCreatedDateAndUserPageable", "wln.pm.getAllPmPageable", "wln.pm.findByMmId",
            "wln.pm.findByCreatedDate", "wln.pm.findByCreatedDatePageable", "wln.pm.findByRrn", "wln.pm.findIsDeletedPmList"}, allEntries = true, beforeInvocation = true)
    public void clearCache() {
        // TODO Auto-generated method stub

    }

    @Override
    @Cacheable(value = "wln.pm.findByRrn", unless = "#result == null")
    public ProjectionMeeting findByRrn(String rrn) {
        // TODO Auto-generated method stub
        return pmDao.findByRrn(rrn);
    }

    @Override
    @Cacheable(value = "wln.pm.findByMmId", unless = "#result == null")
    public List<ProjectionMeeting> findByMmId(MiniMeeting mmId) {
        // TODO Auto-generated method stub
        return pmDao.findByMmId(mmId);
    }

    @Override
    @Cacheable(value = "wln.pm.findById", unless = "#result == null")
    public ProjectionMeeting findById(String pmId) {
        // TODO Auto-generated method stub
        return pmDao.findByPmId(Long.parseLong(pmId));
    }

    @Override
    @Cacheable(value = "wln.pm.findIsDeletedPmList", unless = "#result == null")
    public List<ProjectionMeeting> findIsDeletedPmList() {
        Calendar cal = Calendar.getInstance();
        cal.setTime(new Date());
        cal.add(Calendar.DATE, -7);
        Date dateBefore7Days = cal.getTime();
        return pmDao.findIsDeletedPmId(dateBefore7Days, new Date());
    }

}