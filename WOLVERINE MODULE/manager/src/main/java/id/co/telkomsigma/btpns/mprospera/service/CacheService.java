package id.co.telkomsigma.btpns.mprospera.service;

import id.co.telkomsigma.btpns.mprospera.manager.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service("cacheService")
public class CacheService extends GenericService {

    @Autowired
    AP3RManager ap3RManager;

    @Autowired
    MMManager mmManager;

    @Autowired
    PDKManager pdkManager;

    @Autowired
    SentraManager sentraManager;

    @Autowired
    SWManager swManager;

    @Autowired
    TerminalManager terminalManager;

    @Autowired
    PmManager pmManager;

    @Autowired
    AreaManager areaManager;

    @Autowired
    SDAManager sdaManager;

    @Autowired
    LocationManager locationManager;

    public void clearCache() {
        ap3RManager.clearCache();
        mmManager.clearCache();
        areaManager.clearCache();
        pdkManager.clearCache();
        sentraManager.clearCache();
        swManager.clearCache();
        terminalManager.clearCache();
        pmManager.clearCache();
        sdaManager.clearCache();
        locationManager.clearCache();
    }

}