package id.co.telkomsigma.btpns.mprospera.dao;

import java.util.Date;
import java.util.List;

import id.co.telkomsigma.btpns.mprospera.model.sw.SurveyWawancara;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.jpa.repository.QueryHints;
import org.springframework.data.repository.query.Param;

import javax.persistence.QueryHint;

public interface SWDao extends JpaRepository<SurveyWawancara, String> {

    @Query("SELECT COUNT(m) FROM SurveyWawancara m WHERE m.isDeleted = false AND m.localId is not null")
    int countAll();

    @Query("SELECT COUNT(m) FROM SurveyWawancara m WHERE m.isDeleted = false AND m.status = :status AND m.localId is not null")
    int countAllByStatus(@Param("status") String Status);

    @Query("SELECT m FROM SurveyWawancara m WHERE m.isDeleted = false AND m.localId is not null ORDER BY m.createdBy ASC")
    @QueryHints(@QueryHint(name = "org.hibernate.fetchSize" , value = "100"))
    Page<SurveyWawancara> findAll(Pageable pageable);

    @Query("SELECT m FROM SurveyWawancara m WHERE m.createdDate>=:startDate AND m.createdDate<:endDate AND m.isDeleted = false AND areaId in :areaList AND m.localId is not null ORDER BY m.createdBy ASC")
    @QueryHints(@QueryHint(name = "org.hibernate.fetchSize" , value = "100"))
    Page<SurveyWawancara> findByCreatedDate(@Param("areaList") List<String> kelIdList,
                                            @Param("startDate") Date startDate, @Param("endDate") Date endDate, Pageable pageable);

    @Query("SELECT m FROM SurveyWawancara m WHERE m.createdDate>=:startDate AND m.createdDate<:endDate AND m.isDeleted = false AND m.createdBy in :userList AND m.localId is not null ORDER BY m.createdBy ASC")
    @QueryHints(@QueryHint(name = "org.hibernate.fetchSize" , value = "100"))
    Page<SurveyWawancara> findByCreatedDateAndUser(@Param("userList") List<String> userIdList,
                                                   @Param("startDate") Date startDate, @Param("endDate") Date endDate, Pageable pageable);

    @Query("SELECT m FROM SurveyWawancara m WHERE m.createdDate>=:startDate AND m.createdDate<:endDate AND m.isDeleted = false AND m.createdBy in :userList AND m.localId is not null AND m.status = :status ORDER BY m.createdBy ASC")
    @QueryHints(@QueryHint(name = "org.hibernate.fetchSize" , value = "100"))
    Page<SurveyWawancara> findByCreatedDateAndUserAndStatus(@Param("userList") List<String> userIdList,
                                                            @Param("startDate") Date startDate, @Param("endDate") Date endDate, Pageable pageable, @Param("status") String status);

    @Query("SELECT m FROM SurveyWawancara m WHERE m.createdDate>=:startDate AND m.createdDate<:endDate AND m.isDeleted = false AND m.status = :status AND m.localId is not null ORDER BY m.createdBy ASC")
    @QueryHints(@QueryHint(name = "org.hibernate.fetchSize" , value = "100"))
    Page<SurveyWawancara> findByCreatedDateAndStatus(@Param("status") String status,
                                                     @Param("startDate") Date startDate, @Param("endDate") Date endDate, Pageable pageable);

    @Query("SELECT COUNT(m) FROM SurveyWawancara m WHERE m.swId = :swId AND m.isDeleted = false AND m.localId is not null")
    Integer countBySwId(@Param("swId") Long swId);

    @Query("SELECT a FROM SurveyWawancara a WHERE a.swId  = :swId AND a.isDeleted = false AND a.localId is not null ")
    SurveyWawancara findSwBySwId(@Param("swId") Long swId);

    SurveyWawancara findByLocalId(String localId);

    SurveyWawancara findByRrnAndLocalIdIsNotNull(String rrn);

    Page<SurveyWawancara> findByAreaIdInAndLocalIdIsNotNullAndIsDeleted(List<String> kelIdList, Pageable pageRequest, Boolean deleted);

    Page<SurveyWawancara> findByCreatedByInAndLocalIdIsNotNullAndIsDeleted(List<String> userIdList, Pageable pageRequest, Boolean deleted);

    Page<SurveyWawancara> findByCreatedByInAndLocalIdIsNotNullAndIsDeletedAndStatus(List<String> userIdList, Pageable pageRequest, Boolean deleted, String status);

    Page<SurveyWawancara> findByStatusAndLocalIdIsNotNullAndIsDeleted(String status, Pageable pageRequest, Boolean deleted);

    @Query("SELECT s FROM SurveyWawancara s, SWUserBWMPMapping su WHERE s.swId = su.swId AND su.userId = :userId AND s.status = :status AND s.isDeleted = false ORDER BY s.createdBy ASC")
    @QueryHints(@QueryHint(name = "org.hibernate.fetchSize" , value = "100"))
    Page<SurveyWawancara> findSWWaitingForApprovalNonMS(@Param("userId") Long userId, @Param("status") String Status, Pageable pageRequest);

    @Query("SELECT m FROM SurveyWawancara m WHERE m.createdDate>=:startDate AND m.createdDate<:endDate and m.isDeleted = true")
    @QueryHints(@QueryHint(name = "org.hibernate.fetchSize" , value = "100"))
    List<SurveyWawancara> findIsDeletedSwId(@Param("startDate") Date startDate, @Param("endDate") Date endDate);

}