package id.co.telkomsigma.btpns.mprospera.dao;

import java.util.Date;
import java.util.List;

import id.co.telkomsigma.btpns.mprospera.model.sda.AreaKelurahanDetails;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface AreaKelurahanDetailsDao extends JpaRepository<AreaKelurahanDetails, Long> {

    Integer countByAreaDetailId(long parseLong);

    @Query("SELECT a FROM AreaKelurahanDetails a WHERE a.createdBy in :userList  and a.isDeleted=false")
    Page<AreaKelurahanDetails> searchByParentAreaId(@Param("userList") List<String> userList,
                                                    Pageable pageRequest);

    @Query("SELECT a FROM AreaKelurahanDetails a WHERE a.createdBy in :userList AND a.createdDate>=:startDate AND a.createdDate<:endDate and a.isDeleted=false")
    Page<AreaKelurahanDetails> searchByParentAreaId(@Param("userList") List<String> userList,
                                                    @Param("startDate") Date start, @Param("endDate") Date end, Pageable pageRequest);

}