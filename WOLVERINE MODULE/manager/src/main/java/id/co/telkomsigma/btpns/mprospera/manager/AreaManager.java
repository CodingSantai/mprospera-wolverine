package id.co.telkomsigma.btpns.mprospera.manager;

import id.co.telkomsigma.btpns.mprospera.model.sda.*;

import java.util.List;

public interface AreaManager {

    AreaSubDistrict findSubDistrictById(String areaId);

    AreaKelurahan findKelurahanById(String areaId);

    AreaKelurahan findById(String id);

    List<AreaKelurahan> getKelurahanByParentId(String areaId);

    List<AreaSubDistrict> getSubDistrictByParentId(String areaId);

    List<AreaDistrict> getWismaById(String officeCode);

    AreaDistrict findDistrictById(String districtCode);

    AreaProvince getProvinceById(String areaId);

    void clearCache();

}