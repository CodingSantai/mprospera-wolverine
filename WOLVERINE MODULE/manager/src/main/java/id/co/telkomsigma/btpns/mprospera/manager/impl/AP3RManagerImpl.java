package id.co.telkomsigma.btpns.mprospera.manager.impl;

import java.util.Calendar;
import java.util.Date;
import java.util.List;

import id.co.telkomsigma.btpns.mprospera.constant.WebGuiConstant;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.cache.annotation.Caching;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import id.co.telkomsigma.btpns.mprospera.dao.AP3RDao;
import id.co.telkomsigma.btpns.mprospera.dao.FundedThingsDao;
import id.co.telkomsigma.btpns.mprospera.manager.AP3RManager;
import id.co.telkomsigma.btpns.mprospera.model.sw.AP3R;
import id.co.telkomsigma.btpns.mprospera.model.sw.FundedThings;

@Service("ap3rManager")
public class AP3RManagerImpl implements AP3RManager {

    @Autowired
    private FundedThingsDao fundedThingsDao;

    @Autowired
    private AP3RDao ap3rDao;

    @Override
    @Cacheable(value = "wln.ap3r.countAll", unless = "#result == null")
    public int countAll() {
        // TODO Auto-generated method stub
        return ap3rDao.countAll();
    }

    @Override
    @Caching(evict = {@CacheEvict(value = "hwk.ap3r.findAp3rById", key="#ap3r.ap3rId", beforeInvocation = true),
            @CacheEvict(value = "hwk.ap3r.findTopBySwId", key="#ap3r.swId", beforeInvocation = true),
            @CacheEvict(value = "hwk.ap3r.findAp3rListBySwId", key="#ap3r.swId", beforeInvocation = true),
            @CacheEvict(value = "hwk.ap3r.findByAp3rIdInLoanDeviation", allEntries = true, beforeInvocation = true),
            @CacheEvict(value = "hwk.ap3r.findByAp3rIdInLoanDeviationAndWisma", allEntries = true, beforeInvocation = true),
            @CacheEvict(value = "hwk.ap3r.findByAp3rIdInLoanDeviationWithBatch", allEntries = true, beforeInvocation = true),
            @CacheEvict(value = "hwk.ap3r.findByAp3rIdInLoanDeviationAndWismaWithBatch", allEntries = true, beforeInvocation = true),
            @CacheEvict(value = "hwk.ap3r.findByLocalId", key="#ap3r.localId", beforeInvocation = true),
            @CacheEvict(value = "wln.ap3r.countAll", allEntries = true, beforeInvocation = true),
            @CacheEvict(value = "wln.ap3r.findByAp3rId", key = "#ap3r.ap3rId", beforeInvocation = true),
            @CacheEvict(value = "wln.ap3r.findAllByUser", allEntries = true, beforeInvocation = true),
            @CacheEvict(value = "wln.ap3r.findAllByUserPageable", allEntries = true, beforeInvocation = true),
            @CacheEvict(value = "wln.ap3r.findByCreatedDateAndUser", allEntries = true, beforeInvocation = true),
            @CacheEvict(value = "wln.ap3r.findByCreatedDateAndUserPageable", allEntries = true, beforeInvocation = true),
            @CacheEvict(value = "wln.ap3r.findAllByUserMS", allEntries = true, beforeInvocation = true),
            @CacheEvict(value = "wln.ap3r.findAllByUserMSPageable", allEntries = true, beforeInvocation = true),
            @CacheEvict(value = "wln.ap3r.findByCreatedDateAndUserMS", allEntries = true, beforeInvocation = true),
            @CacheEvict(value = "wln.ap3r.findByCreatedDateAndUserMSPageable", allEntries = true, beforeInvocation = true),
            @CacheEvict(value = "wln.ap3r.findForDeviation", allEntries = true, beforeInvocation = true),
            @CacheEvict(value = "wln.ap3r.findForDeviationPageable", allEntries = true, beforeInvocation = true),
            @CacheEvict(value = "wln.ap3r.findForDeviationByCreatedDate", allEntries = true, beforeInvocation = true),
            @CacheEvict(value = "wln.ap3r.findForDeviationByCreatedDatePageable", allEntries = true, beforeInvocation = true),
            @CacheEvict(value = "wln.ap3r.findByLocalId", key = "#ap3r.localId", beforeInvocation = true),
            @CacheEvict(value = "wln.ap3r.findIsDeletedAp3rList", allEntries = true, beforeInvocation = true),
            @CacheEvict(value = "wln.ap3r.findAp3rListBySwId", allEntries = true, beforeInvocation = true)})
    public void save(AP3R ap3r) {
        // TODO Auto-generated method stub
        ap3rDao.save(ap3r);
    }

    @Override
    @Cacheable(value = "wln.ap3r.findByAp3rId", unless = "#result == null")
    public AP3R findByAp3rId(Long id) {
        return ap3rDao.findByAp3rIdAndLocalIdIsNotNullAndIsDeleted(id, false);
    }

    @Override
    @Cacheable(value = "wln.ap3r.findAllByUser", unless = "#result == null")
    public Page<AP3R> findAllByUser(List<String> userIdList) {
        // TODO Auto-generated method stub
        return ap3rDao.findByCreatedByInAndLocalIdIsNotNullAndIsDeleted(userIdList, new PageRequest(0, countAll()), false);
    }

    @Override
    @Cacheable(value = "wln.ap3r.findAllByUserPageable", unless = "#result == null")
    public Page<AP3R> findAllByUserPageable(List<String> userIdList, PageRequest pageRequest) {
        // TODO Auto-generated method stub
        return ap3rDao.findByCreatedByInAndLocalIdIsNotNullAndIsDeleted(userIdList, pageRequest, false);
    }

    @Override
    @Cacheable(value = "wln.ap3r.findByCreatedDateAndUser", unless = "#result == null")
    public Page<AP3R> findByCreatedDateAndUser(List<String> userIdList, Date startDate, Date endDate) {
        // TODO Auto-generated method stub
        Calendar cal = Calendar.getInstance();
        Calendar calStart = Calendar.getInstance();
        cal.setTime(endDate);
        calStart.setTime(startDate);
        cal.add(Calendar.DATE, 1);
        calStart.add(Calendar.DATE, -7);
        return ap3rDao.findByCreatedDateAndUser(userIdList, calStart.getTime(), cal.getTime(), new PageRequest(0, Integer.MAX_VALUE));
    }

    @Override
    @Cacheable(value = "wln.ap3r.findByCreatedDateAndUserPageable", unless = "#result == null")
    public Page<AP3R> findByCreatedDateAndUserPageable(List<String> userIdList, Date startDate,
                                                       Date endDate, PageRequest pageRequest) {
        // TODO Auto-generated method stub
        Calendar cal = Calendar.getInstance();
        Calendar calStart = Calendar.getInstance();
        cal.setTime(endDate);
        calStart.setTime(startDate);
        cal.add(Calendar.DATE, 1);
        calStart.add(Calendar.DATE, -7);
        return ap3rDao.findByCreatedDateAndUser(userIdList, calStart.getTime(), cal.getTime(), pageRequest);
    }

    @Override
    @Cacheable(value = "wln.ap3r.findAllByUserMS", unless = "#result == null")
    public Page<AP3R> findAllByUserMS(List<String> userIdList) {
        return ap3rDao.findByCreatedByInAndLocalIdIsNotNullAndIsDeletedAndStatus(userIdList, new PageRequest(0, countAll()), false, WebGuiConstant.STATUS_WAITING_APPROVAL);
    }

    @Override
    @Cacheable(value = "wln.ap3r.findAllByUserMSPageable", unless = "#result == null")
    public Page<AP3R> findAllByUserMSPageable(List<String> userIdList, PageRequest pageRequest) {
        return ap3rDao.findByCreatedByInAndLocalIdIsNotNullAndIsDeletedAndStatus(userIdList, pageRequest, false, WebGuiConstant.STATUS_WAITING_APPROVAL);
    }

    @Override
    @Cacheable(value = "wln.ap3r.findByCreatedDateAndUserMS", unless = "#result == null")
    public Page<AP3R> findByCreatedDateAndUserMS(List<String> userIdList, Date startDate, Date endDate) {
        Calendar cal = Calendar.getInstance();
        Calendar calStart = Calendar.getInstance();
        cal.setTime(endDate);
        calStart.setTime(startDate);
        cal.add(Calendar.DATE, 1);
        calStart.add(Calendar.DATE, -7);
        return ap3rDao.findByCreatedDateAndUserAndStatus(userIdList, calStart.getTime(), cal.getTime(), new PageRequest(0, Integer.MAX_VALUE), WebGuiConstant.STATUS_WAITING_APPROVAL);
    }

    @Override
    @Cacheable(value = "wln.ap3r.findByCreatedDateAndUserMSPageable", unless = "#result == null")
    public Page<AP3R> findByCreatedDateAndUserMSPageable(List<String> userIdList, Date startDate, Date endDate, PageRequest pageRequest) {
        Calendar cal = Calendar.getInstance();
        Calendar calStart = Calendar.getInstance();
        cal.setTime(endDate);
        calStart.setTime(startDate);
        cal.add(Calendar.DATE, 1);
        calStart.add(Calendar.DATE, -7);
        return ap3rDao.findByCreatedDateAndUserAndStatus(userIdList, calStart.getTime(), cal.getTime(), pageRequest, WebGuiConstant.STATUS_WAITING_APPROVAL);
    }

    @Override
    @Cacheable(value = "wln.ap3r.findForDeviation", unless = "#result == null")
    public Page<AP3R> findForDeviation() {
        return ap3rDao.findAllForDeviation(new PageRequest(0, countAll()));
    }

    @Override
    @Cacheable(value = "wln.ap3r.findForDeviationPageable", unless = "#result == null")
    public Page<AP3R> findForDeviationPageable(PageRequest pageRequest) {
        return ap3rDao.findAllForDeviation(pageRequest);
    }

    @Override
    @Cacheable(value = "wln.ap3r.findForDeviationByCreatedDate", unless = "#result == null")
    public Page<AP3R> findForDeviationByCreatedDate(Date startDate, Date endDate) {
        Calendar cal = Calendar.getInstance();
        Calendar calStart = Calendar.getInstance();
        cal.setTime(endDate);
        calStart.setTime(startDate);
        cal.add(Calendar.DATE, 1);
        calStart.add(Calendar.DATE, -7);
        return ap3rDao.findForDeviationByCreatedDate(calStart.getTime(), cal.getTime(), new PageRequest(0, Integer.MAX_VALUE));
    }

    @Override
    @Cacheable(value = "wln.ap3r.findForDeviationByCreatedDatePageable", unless = "#result == null")
    public Page<AP3R> findForDeviationByCreatedDatePageable(Date startDate, Date endDate, PageRequest pageRequest) {
        Calendar cal = Calendar.getInstance();
        Calendar calStart = Calendar.getInstance();
        cal.setTime(endDate);
        calStart.setTime(startDate);
        cal.add(Calendar.DATE, 1);
        calStart.add(Calendar.DATE, -7);
        return ap3rDao.findForDeviationByCreatedDate(calStart.getTime(), cal.getTime(), pageRequest);
    }

    @Cacheable(value = "wln.ap3r.findByLocalId", unless = "#result == null")
    public AP3R findByLocalId(String localId) {
        return ap3rDao.findByLocalId(localId);
    }

    @Override
    public AP3R findByLocalIdAndIsDeleted(String localId) {
        return ap3rDao.findByLocalIdAndIsDeleted(localId, false);
    }

    @Override
    @Cacheable(value = "wln.ap3r.findIsDeletedAp3rList", unless = "#result == null")
    public List<AP3R> findIsDeletedAp3rList() {
        Calendar cal = Calendar.getInstance();
        cal.setTime(new Date());
        cal.add(Calendar.DATE, -7);
        Date dateBefore7Days = cal.getTime();
        return ap3rDao.findIsDeletedAp3rId(dateBefore7Days, new Date());
    }

    @Override
    @Cacheable(value = "wln.ap3r.findAp3rListBySwId", unless = "#result == null")
    public List<AP3R> findAp3rListBySwId(Long swId) {
        return ap3rDao.findBySwIdAndIsDeleted(swId, false);
    }

    @Override
    @Caching(evict = {@CacheEvict(value = "wln.things.findByAp3rId", allEntries = true, beforeInvocation = true),
            @CacheEvict(value = "hwk.things.findByAp3rId", allEntries = true, beforeInvocation = true)})
    public void save(FundedThings product) {
        // TODO Auto-generated method stub
        fundedThingsDao.save(product);
    }

    @Override
    @Caching(evict = {@CacheEvict(value = "wln.things.findByAp3rId", allEntries = true, beforeInvocation = true),
            @CacheEvict(value = "hwk.things.findByAp3rId", allEntries = true, beforeInvocation = true)})
    public void delete(FundedThings products) {
        fundedThingsDao.delete(products);
    }

    @Override
    @Cacheable(value = "wln.things.findByAp3rId", unless = "#result == null")
    public List<FundedThings> findByAp3rId(AP3R ap3r) {
        // TODO Auto-generated method stub
        return fundedThingsDao.findByAp3rId(ap3r);
    }

    @Override
    @CacheEvict(allEntries = true, cacheNames = {"wln.ap3r.countAll", "wln.things.findByAp3rId",
            "wln.ap3r.findByAp3rId", "wln.ap3r.findAllByUser", "wln.ap3r.findAllByUserPageable",
            "wln.ap3r.findByCreatedDateAndUser", "wln.ap3r.findByCreatedDateAndUserPageable",
            "wln.ap3r.findAllByUserMS", "wln.ap3r.findAllByUserMSPageable", "wln.ap3r.findByCreatedDateAndUserMS",
            "wln.ap3r.findByCreatedDateAndUserMSPageable", "wln.ap3r.findForDeviation", "wln.ap3r.findForDeviationPageable",
            "wln.ap3r.findForDeviationByCreatedDate", "wln.ap3r.findForDeviationByCreatedDatePageable",
            "wln.ap3r.findByLocalId", "wln.ap3r.findAp3rListBySwId"})
    public void clearCache() {

    }

}