package id.co.telkomsigma.btpns.mprospera.service;

import com.hazelcast.core.HazelcastInstance;
import com.hazelcast.core.IMap;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

@Service
public class HazelcastClientService {

    @Autowired
    @Qualifier("hzInstance")
    private HazelcastInstance hazelcastInstance;

    public boolean checkExist(String map, String key) throws Exception {
        if (hazelcastInstance == null) {
            throw new Exception("Hazelcast instance not found");
        }
        IMap<String, Object> mapInstance = hazelcastInstance.getMap(map);
        Object obj = mapInstance.get(key);
        if (obj != null) {
            return true;
        } else {
            return false;
        }
    }

    public void insertKeyValueToMap(String map, String key, Object value) {
        try {
            if (hazelcastInstance == null) {
                throw new Exception("Hazelcast instance not found");
            }
            IMap<String, Object> mapInstance = hazelcastInstance.getMap(map);
            mapInstance.put(key, value);
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }

}