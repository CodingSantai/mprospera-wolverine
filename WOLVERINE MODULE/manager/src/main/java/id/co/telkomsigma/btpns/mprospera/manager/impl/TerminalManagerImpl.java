package id.co.telkomsigma.btpns.mprospera.manager.impl;

import id.co.telkomsigma.btpns.mprospera.dao.TerminalDao;
import id.co.telkomsigma.btpns.mprospera.manager.TerminalManager;
import id.co.telkomsigma.btpns.mprospera.model.terminal.Terminal;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.cache.annotation.Caching;
import org.springframework.stereotype.Service;

/**
 * Created by Dzulfiqar on 11/11/15.
 */
@Service("terminalManager")
public class TerminalManagerImpl implements TerminalManager {

    @Autowired
    TerminalDao terminalDao;

    @Cacheable(value = "wln.terminal.getTerminalByImei", unless = "#result == null")
    public Terminal getTerminalByImei(String imei) {
        return terminalDao.findByImei(imei);
    }

    @Override
    @Caching(evict = {@CacheEvict(value = "vsn.terminal.allTerminal", allEntries = true, beforeInvocation = true),
            @CacheEvict(value = "vsn.terminal.terminalByImei", key = "#terminal.imei", beforeInvocation = true),
            @CacheEvict(value = "wln.terminal.terminalByImei", key = "#terminal.imei", beforeInvocation = true),
            @CacheEvict(value = "hwk.terminal.terminalByImei", key = "#terminal.imei", beforeInvocation = true),
            @CacheEvict(value = "hwk.terminal.allTerminal", allEntries = true, beforeInvocation = true)})
    public Terminal updateTerminal(Terminal terminal) {
        terminalDao.save(terminal);
        return terminal;
    }

    @Override
    @CacheEvict(value = {"wln.terminal.getTerminalByImei"}, allEntries = true, beforeInvocation = true)
    public void clearCache() {
        // TODO Auto-generated method stub
    }

}