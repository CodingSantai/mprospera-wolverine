package id.co.telkomsigma.btpns.mprospera.manager.impl;

import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.cache.annotation.Caching;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import id.co.telkomsigma.btpns.mprospera.dao.PDKDao;
import id.co.telkomsigma.btpns.mprospera.dao.PDKDetailDao;
import id.co.telkomsigma.btpns.mprospera.manager.PDKManager;
import id.co.telkomsigma.btpns.mprospera.model.pdk.PelatihanDasarKeanggotaan;

@SuppressWarnings("RedundantIfStatement")
@Service("pdkManager")
public class PDKManagerImpl implements PDKManager {

    @Autowired
    PDKDao pdkDao;

    @Autowired
    PDKDetailDao pdkDetailDao;

    @Override
    @Cacheable(value = "wln.pdk.countAllPDK", unless = "#result == null")
    public int countAll() {
        // TODO Auto-generated method stub
        return pdkDao.countAll();
    }

    @Override
    @Cacheable(value = "wln.pdk.findAllPDK", unless = "#result == null")
    public Page<PelatihanDasarKeanggotaan> findAll(List<String> userList) {
        // TODO Auto-generated method stub
        return pdkDao.findAll(userList, new PageRequest(0, countAll()));
    }

    @Override
    @Cacheable(value = "wln.pdk.findAllPDKPageable", unless = "#result == null")
    public Page<PelatihanDasarKeanggotaan> findAllPageable(List<String> userList, PageRequest pageRequest) {
        // TODO Auto-generated method stub
        return pdkDao.findAll(userList, pageRequest);
    }

    @Override
    @Cacheable(value = "wln.pdk.findPDKByCreatedDate", unless = "#result == null")
    public Page<PelatihanDasarKeanggotaan> findByCreatedDate(List<String> userList, Date startDate, Date endDate) {
        // TODO Auto-generated method stub
        Calendar cal = Calendar.getInstance();
        Calendar calStart = Calendar.getInstance();
        cal.setTime(endDate);
        calStart.setTime(startDate);
        cal.add(Calendar.DATE, 1);
        calStart.add(Calendar.DATE, -7);
        return pdkDao.findByCreatedDate(userList, calStart.getTime(), cal.getTime(), new PageRequest(0, Integer.MAX_VALUE));
    }

    @Override
    @Cacheable(value = "wln.pdk.findPDKByCreatedDatePageable", unless = "#result == null")
    public Page<PelatihanDasarKeanggotaan> findByCreatedDatePageable(List<String> userList, Date startDate,
                                                                     Date endDate, PageRequest pageRequest) {
        // TODO Auto-generated method stub
        Calendar cal = Calendar.getInstance();
        Calendar calStart = Calendar.getInstance();
        cal.setTime(endDate);
        calStart.setTime(startDate);
        cal.add(Calendar.DATE, 1);
        calStart.add(Calendar.DATE, -7);
        return pdkDao.findByCreatedDate(userList, calStart.getTime(), cal.getTime(), pageRequest);
    }

    @Override
    @Cacheable(value = "wln.pdk.isValidpdk", unless = "#result == null")
    public Boolean isValidpdk(String pdkId) {
        // TODO Auto-generated method stub
        Integer count = pdkDao.countByPdkId(Long.parseLong(pdkId));
        if (count > 0)
            return true;
        else
            return false;
    }

    @Override
    @org.springframework.transaction.annotation.Transactional
    @Caching(evict = {@CacheEvict(value = "wln.pdk.countAllPDK", allEntries = true, beforeInvocation = true),
            @CacheEvict(value = "wln.pdk.findAllPDK", allEntries = true, beforeInvocation = true),
            @CacheEvict(value = "wln.pdk.findAllPDKPageable", allEntries = true, beforeInvocation = true),
            @CacheEvict(value = "wln.pdk.findPDKByCreatedDate", allEntries = true, beforeInvocation = true),
            @CacheEvict(value = "wln.pdk.findPDKByCreatedDatePageable", allEntries = true, beforeInvocation = true),
            @CacheEvict(value = "wln.pdk.findPDKByRrn", allEntries = true, beforeInvocation = true),
            @CacheEvict(value = "wln.pdk.isValidpdk", key = "#pelatihanDasarKeanggotaan.pdkId", allEntries = true, beforeInvocation = true),
            @CacheEvict(value = "wln.pdk.findIsDeletedPdkList", allEntries = true, beforeInvocation = true),
            @CacheEvict(value = "hwk.pdk.isValidpdk", key = "#pelatihanDasarKeanggotaan.pdkId", beforeInvocation = true),
            @CacheEvict(value = "hwk.pdk.findById", key = "#pelatihanDasarKeanggotaan.pdkId", beforeInvocation = true)})
    public void save(PelatihanDasarKeanggotaan pelatihanDasarKeanggotaan) {
        if (pelatihanDasarKeanggotaan.getPdkId() != null)
            pdkDetailDao.cleanPdkId(pelatihanDasarKeanggotaan);
        pdkDao.save(pelatihanDasarKeanggotaan);
    }

    @Override
    @CacheEvict(value = {"wln.pdk.countAllPDK", "wln.pdk.findAllPDK", "wln.pdk.findAllPDKPageable",
            "wln.pdk.findPDKByCreatedDate", "wln.pdk.findPDKByCreatedDatePageable",
            "wln.pdk.isValidpdk", "wln.pdk.findPDKByRrn", "wln.pdk.findIsDeletedPdkList"}, allEntries = true, beforeInvocation = true)
    public void clearCache() {
        // TODO Auto-generated method stub

    }

    @Override
    @Cacheable(value = "wln.pdk.findPDKByRrn", unless = "#result == null")
    public PelatihanDasarKeanggotaan findByRrn(String rrn) {
        // TODO Auto-generated method stub
        return pdkDao.findByRrn(rrn);
    }

    @Override
    @Cacheable(value = "wln.pdk.findIsDeletedPdkList", unless = "#result == null")
    public List<PelatihanDasarKeanggotaan> findIsDeletedPdkList() {
        Calendar cal = Calendar.getInstance();
        cal.setTime(new Date());
        cal.add(Calendar.DATE, -7);
        Date dateBefore7Days = cal.getTime();
        return pdkDao.findIsDeletedPdkId(dateBefore7Days, new Date());
    }

}