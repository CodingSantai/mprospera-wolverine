package id.co.telkomsigma.btpns.mprospera.manager.impl;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import id.co.telkomsigma.btpns.mprospera.dao.GroupDao;
import id.co.telkomsigma.btpns.mprospera.dao.SentraDao;
import id.co.telkomsigma.btpns.mprospera.manager.SentraManager;
import id.co.telkomsigma.btpns.mprospera.model.sentra.Sentra;

@SuppressWarnings({"unchecked", "RedundantIfStatement"})
@Service("sentraManager")
public class SentraManagerImpl implements SentraManager {

    @Autowired
    private SentraDao sentraDao;

    @Autowired
    private GroupDao groupDao;

    @Override
    @Cacheable(value = "wln.sentra.findByCreatedDate", unless = "#result == null")
    public Page<Sentra> findByCreatedDate(String username, String loc, Date startDate, Date endDate) {
        // TODO Auto-generated method stub
        if (username == null) {
            return sentraDao.findByCreatedDateWithLoc(loc, startDate, endDate, new PageRequest(0, Integer.MAX_VALUE));
        } else {
            return sentraDao.findByCreatedDate(username, startDate, endDate, new PageRequest(0, Integer.MAX_VALUE));
        }
    }

    @Override
    @Cacheable(value = "wln.sentra.findByCreatedDatePageable", unless = "#result == null")
    public Page<Sentra> findByCreatedDatePageable(String username, String loc, Date startDate, Date endDate,
                                                  PageRequest pageRequest) {
        // TODO Auto-generated method stub
        if (username == null) {
            return sentraDao.findByCreatedDateWithLoc(loc, startDate, endDate, pageRequest);
        } else {
            return sentraDao.findByCreatedDate(username, startDate, endDate, pageRequest);
        }
    }

    @Override
    @CacheEvict(allEntries = true, cacheNames = {"wln.sentra.findByCreatedDate", "wln.sentra.findByCreatedDatePageable",
            "wln.sentra.findByRrn", "wln.sentra.findAll"})
    public void clearCache() {
        // TODO Auto-generated method stub
    }

    @Override
    @Cacheable(value = "wln.sentra.findByRrn", unless = "#result == null")
    public Sentra findByRrn(String rrn) {
        // TODO Auto-generated method stub
        return sentraDao.findByRrn(rrn);
    }

    @Override
    @Cacheable(value = "wln.sentra.findAll", unless = "#result == null")
    public List<Sentra> findAll() {
        // TODO Auto-generated method stub
        return sentraDao.findAll();
    }

}