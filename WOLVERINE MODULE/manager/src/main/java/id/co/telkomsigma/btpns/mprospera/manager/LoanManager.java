package id.co.telkomsigma.btpns.mprospera.manager;

import java.util.Date;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import id.co.telkomsigma.btpns.mprospera.model.customer.Customer;
import id.co.telkomsigma.btpns.mprospera.model.loan.Loan;
import id.co.telkomsigma.btpns.mprospera.model.loan.LoanDeviation;

public interface LoanManager {

    Page<Loan> findByCreatedDate(Date startDate, Date endDate, String officeId);

    Page<Loan> findByCreatedDatePageable(Date startDate, Date endDate, PageRequest pageRequest, String officeId);

    void clearCache();

    Loan findById(long parseLong);

    LoanDeviation findDeviationByAp3rId(Long ap3rId);

    Loan findByAp3rAndCustomer(Long ap3rId, Customer customer);

}