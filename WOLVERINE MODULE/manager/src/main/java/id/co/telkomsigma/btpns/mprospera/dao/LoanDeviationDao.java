package id.co.telkomsigma.btpns.mprospera.dao;

import id.co.telkomsigma.btpns.mprospera.model.loan.LoanDeviation;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Created by Dzulfiqar on 25/04/2017.
 */
public interface LoanDeviationDao extends JpaRepository<LoanDeviation, Long> {

    LoanDeviation findByAp3rIdAndIsDeletedFalse(Long ap3rId);

}