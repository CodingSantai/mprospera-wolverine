package id.co.telkomsigma.btpns.mprospera.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import id.co.telkomsigma.btpns.mprospera.model.sw.AWGMBuyThings;
import id.co.telkomsigma.btpns.mprospera.model.sw.SurveyWawancara;

public interface AWGMDao extends JpaRepository<AWGMBuyThings, Long> {

    List<AWGMBuyThings> findBySwId(SurveyWawancara sw);

}