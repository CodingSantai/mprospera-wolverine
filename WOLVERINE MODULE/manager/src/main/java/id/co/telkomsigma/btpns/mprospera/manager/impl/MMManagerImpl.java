package id.co.telkomsigma.btpns.mprospera.manager.impl;

import java.util.Calendar;
import java.util.Date;
import java.util.List;

import id.co.telkomsigma.btpns.mprospera.dao.MMDao;
import id.co.telkomsigma.btpns.mprospera.manager.MMManager;
import id.co.telkomsigma.btpns.mprospera.model.mm.MiniMeeting;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.cache.annotation.Caching;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

@SuppressWarnings("RedundantIfStatement")
@Service("mmManager")
public class MMManagerImpl implements MMManager {

    @Autowired
    private MMDao mmDao;

    @Override
    @Cacheable(value = "wln.mm.countAll", unless = "#result == null")
    public int countAll() {
        return mmDao.countAll();
    }

    @Override
    @Cacheable(value = "wln.mm.findAll", unless = "#result == null")
    public Page<MiniMeeting> findAll(List<String> kelIdList) {
        return mmDao.findByAreaIdIn(kelIdList, new PageRequest(0, Integer.MAX_VALUE));
    }

    @Override
    @Cacheable(value = "wln.mm.findAllMM", unless = "#result == null")
    public Page<MiniMeeting> findAllMM() {
        return mmDao.findAll(new PageRequest(0, Integer.MAX_VALUE));
    }

    @Override
    @Cacheable(value = "wln.mm.findAllMMPageable", unless = "#result == null")
    public Page<MiniMeeting> findAllMMPageable(PageRequest pageRequest) {
        return mmDao.findAll(pageRequest);
    }

    @Override
    @Cacheable(value = "wln.mm.findAllByCreatedDate", unless = "#result == null")
    public Page<MiniMeeting> findAllByCreatedDate(Date startDate, Date endDate) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(endDate);
        cal.add(Calendar.DATE, 1);
        return mmDao.findAllByCreatedDate(startDate, cal.getTime(), new PageRequest(0, Integer.MAX_VALUE));
    }

    @Override
    @Cacheable(value = "wln.mm.findAllByCreatedDatePageable", unless = "#result == null")
    public Page<MiniMeeting> findAllByCreatedDatePageable(Date startDate, Date endDate, PageRequest pageRequest) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(endDate);
        cal.add(Calendar.DATE, 1);
        return mmDao.findAllByCreatedDate(startDate, cal.getTime(), pageRequest);
    }

    @Override
    @Cacheable(value = "wln.mm.findAllMMByUser", unless = "#result == null")
    public Page<MiniMeeting> findAllMMByUser(List<String> userIdList) {
        return mmDao.findAllByUser(userIdList, new PageRequest(0, Integer.MAX_VALUE));
    }

    @Override
    @Cacheable(value = "wln.mm.findAllMMByUserPageable", unless = "#result == null")
    public Page<MiniMeeting> findAllMMByUserPageable(List<String> userIdList, PageRequest pageRequest) {
        return mmDao.findAllByUser(userIdList, pageRequest);
    }

    @Override
    @Cacheable(value = "wln.mm.findAllByCreatedDateAndUser", unless = "#result == null")
    public Page<MiniMeeting> findAllByCreatedDateAndUser(List<String> userIdList, Date startDate, Date endDate) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(endDate);
        cal.add(Calendar.DATE, 1);
        return mmDao.findAllByCreatedDateAndUser(userIdList, startDate, cal.getTime(), new PageRequest(0, Integer.MAX_VALUE));
    }

    @Override
    @Cacheable(value = "wln.mm.findAllByCreatedDateAndUserPageable", unless = "#result == null")
    public Page<MiniMeeting> findAllByCreatedDateAndUserPageable(List<String> userIdList, Date startDate, Date endDate, PageRequest pageRequest) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(endDate);
        cal.add(Calendar.DATE, 1);
        return mmDao.findAllByCreatedDateAndUser(userIdList, startDate, cal.getTime(), pageRequest);
    }

    @Override
    @Cacheable(value = "wln.mm.findAllPageable", unless = "#result == null")
    public Page<MiniMeeting> findAllPageable(List<String> kelIdList, PageRequest pageRequest) {
        return mmDao.findByAreaIdIn(kelIdList, pageRequest);
    }

    @Override
    @Cacheable(value = "wln.mm.findByCreatedDate", unless = "#result == null")
    public Page<MiniMeeting> findByCreatedDate(List<String> kelIdList, Date startDate, Date endDate) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(endDate);
        cal.add(Calendar.DATE, 1);
        return mmDao.findByCreatedDate(startDate, cal.getTime(), kelIdList, new PageRequest(0, Integer.MAX_VALUE));
    }

    @Override
    @Cacheable(value = "wln.mm.findByCreatedDatePageable", unless = "#result == null")
    public Page<MiniMeeting> findByCreatedDatePageable(List<String> kelIdList, Date startDate, Date endDate,
                                                       PageRequest pageRequest) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(endDate);
        cal.add(Calendar.DATE, 1);
        return mmDao.findByCreatedDate(startDate, cal.getTime(), kelIdList, pageRequest);
    }

    @Override
    @Caching(evict = {@CacheEvict(value = "wln.mm.countAll", allEntries = true, beforeInvocation = true),
            @CacheEvict(value = "wln.mm.findAll", allEntries = true, beforeInvocation = true),
            @CacheEvict(value = "wln.mm.findAllMM", beforeInvocation = true, allEntries = true),
            @CacheEvict(value = "wln.mm.findAllMMPageable", beforeInvocation = true, allEntries = true),
            @CacheEvict(value = "wln.mm.findAllByCreatedDate", beforeInvocation = true, allEntries = true),
            @CacheEvict(value = "wln.mm.findAllByCreatedDatePageable", beforeInvocation = true, allEntries = true),
            @CacheEvict(value = "wln.mm.findAllMMByUser", beforeInvocation = true, allEntries = true),
            @CacheEvict(value = "wln.mm.findAllMMByUserPageable", allEntries = true, beforeInvocation = true),
            @CacheEvict(value = "wln.mm.findAllByCreatedDateAndUser", allEntries = true, beforeInvocation = true),
            @CacheEvict(value = "wln.mm.findAllByCreatedDateAndUserPageable", allEntries = true, beforeInvocation = true),
            @CacheEvict(value = "wln.mm.findAllPageable", allEntries = true, beforeInvocation = true),
            @CacheEvict(value = "wln.mm.findByCreatedDate", allEntries = true, beforeInvocation = true),
            @CacheEvict(value = "wln.mm.findByCreatedDatePageable", allEntries = true, beforeInvocation = true),
            @CacheEvict(value = "wln.mm.isValidMm", allEntries = true, beforeInvocation = true),
            @CacheEvict(value = "wln.mm.findByRrn", allEntries = true, beforeInvocation = true),
            @CacheEvict(value = "wln.mm.findIsDeletedMmList", allEntries = true, beforeInvocation = true)})
    public void save(MiniMeeting miniMeeting) {
        mmDao.save(miniMeeting);
    }

    @Override
    @Cacheable(value = "wln.mm.isValidMm", unless = "#result == null")
    public Boolean isValidMm(String mmId) {
        Integer count = mmDao.countByMmId(Long.parseLong(mmId));
        if (count > 0)
            return true;
        else
            return false;
    }

    @Override
    @CacheEvict(value = {"wln.mm.countAll", "wln.mm.findAll", "wln.mm.findAllMM", "wln.mm.findAllMMPageable",
            "wln.mm.findAllByCreatedDate", "wln.mm.findAllByCreatedDatePageable", "wln.mm.findAllMMByUser",
            "wln.mm.findAllMMByUserPageable", "wln.mm.findAllByCreatedDateAndUser", "wln.mm.findAllByCreatedDateAndUserPageable",
            "wln.mm.findAllPageable", "wln.mm.findByCreatedDate", "wln.mm.findByCreatedDatePageable",
            "wln.mm.isValidMm", "wln.mm.findByRrn", "wln.mm.findIsDeletedMmList"}, allEntries = true, beforeInvocation = true)
    public void clearCache() {
        // TODO Auto-generated method stub

    }

    @Override
    @Cacheable(value = "wln.mm.findByRrn", unless = "#result == null")
    public MiniMeeting findByRrn(String rrn) {
        // TODO Auto-generated method stub
        return mmDao.findByRrn(rrn);
    }

    @Override
    @Cacheable(value = "wln.mm.findIsDeletedMmList", unless = "#result == null")
    public List<MiniMeeting> findIsDeletedMmList() {
        Calendar cal = Calendar.getInstance();
        cal.setTime(new Date());
        cal.add(Calendar.DATE, -7);
        Date dateBefore7Days = cal.getTime();
        return mmDao.findIsDeletedMmId(dateBefore7Days, new Date());
    }

}