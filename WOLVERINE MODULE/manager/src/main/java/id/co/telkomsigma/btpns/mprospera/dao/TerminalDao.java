package id.co.telkomsigma.btpns.mprospera.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import id.co.telkomsigma.btpns.mprospera.model.terminal.Terminal;

/**
 * Created by Dzulfiqar on 11/11/15.
 */
public interface TerminalDao extends JpaRepository<Terminal, Long> {

    Terminal findByImei(String imei);

}