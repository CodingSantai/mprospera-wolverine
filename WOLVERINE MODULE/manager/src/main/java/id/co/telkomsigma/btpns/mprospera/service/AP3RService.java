package id.co.telkomsigma.btpns.mprospera.service;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import id.co.telkomsigma.btpns.mprospera.manager.AP3RManager;
import id.co.telkomsigma.btpns.mprospera.manager.UserManager;
import id.co.telkomsigma.btpns.mprospera.model.sw.AP3R;
import id.co.telkomsigma.btpns.mprospera.model.sw.FundedThings;
import id.co.telkomsigma.btpns.mprospera.model.user.User;

@Service("ap3rService")
public class AP3RService extends GenericService {

    @Autowired
    private UserManager userManager;

    @Autowired
    private AP3RManager ap3rManager;

    private SimpleDateFormat formatter = new SimpleDateFormat("yyyyMMdd");

    public void save(AP3R ap3r) {
        ap3rManager.save(ap3r);
    }

    public AP3R findByAp3rId(Long id) {
        return ap3rManager.findByAp3rId(id);
    }

    public Page<AP3R> getAp3rByUser(String username, String page, String getCountData, String startDate,
                                    String endDate) throws ParseException {
        if (getCountData == null)
            page = null;
        if ((startDate == null || startDate.equals("")) && endDate != null)
            startDate = endDate;
        if (endDate == null || endDate.equals("") && startDate != null)
            endDate = startDate;
        int pageNumber = Integer.parseInt(page) - 1;
        User userSW = userManager.getUserByUsername(username);
        List<User> userListWisma = userManager.getUserByLocId(userSW.getOfficeCode());
        List<String> userIdList = new ArrayList<>();
        for (User user : userListWisma) {
            userIdList.add(user.getUsername());
        }
        if (startDate == null || startDate.equals(""))
            if (page == null || page.equals(""))
                if (userSW.getRoleUser().equals("3")) {
                    return ap3rManager.findForDeviation();
                } else
                    return ap3rManager.findAllByUser(userIdList);
            else {
                if (userSW.getRoleUser().equals("3")) {
                    return ap3rManager.findForDeviationPageable(new PageRequest(pageNumber, Integer.parseInt(getCountData)));
                } else
                    return ap3rManager.findAllByUserPageable(userIdList,
                            new PageRequest(pageNumber, Integer.parseInt(getCountData)));
            }
        else if (page == null || page.equals(""))
            if (userSW.getRoleUser().equals("3")) {
                return ap3rManager.findForDeviationByCreatedDate(formatter.parse(startDate), formatter.parse(endDate));
            } else
                return ap3rManager.findByCreatedDateAndUser(userIdList, formatter.parse(startDate), formatter.parse(endDate));
        else if (userSW.getRoleUser().equals("3")) {
            return ap3rManager.findForDeviationByCreatedDatePageable(formatter.parse(startDate),
                    formatter.parse(endDate), new PageRequest(pageNumber, Integer.parseInt(getCountData)));
        } else
            return ap3rManager.findByCreatedDateAndUserPageable(userIdList, formatter.parse(startDate),
                    formatter.parse(endDate), new PageRequest(pageNumber, Integer.parseInt(getCountData)));
    }

    public Page<AP3R> getAp3rByUserMS(String username, String page, String getCountData, String startDate,
                                      String endDate) throws ParseException {
        if (getCountData == null)
            page = null;
        if ((startDate == null || startDate.equals("")) && endDate != null)
            startDate = endDate;
        if (endDate == null || endDate.equals("") && startDate != null)
            endDate = startDate;
        User userSW = userManager.getUserByUsername(username);
        List<User> userListWisma = userManager.getUserByLocId(userSW.getOfficeCode());
        List<String> userIdList = new ArrayList<>();
        for (User user : userListWisma) {
            userIdList.add(user.getUsername());
        }
        if (startDate == null || startDate.equals(""))
            if (page == null || page.equals("")) {
                return ap3rManager.findAllByUserMS(userIdList);
            } else {
            	int pageNumber = Integer.parseInt(page) - 1;
                return ap3rManager.findAllByUserMSPageable(userIdList,
                        new PageRequest(pageNumber, Integer.parseInt(getCountData)));
            }
        else if (page == null || page.equals("")) {
            return ap3rManager.findByCreatedDateAndUserMS(userIdList, formatter.parse(startDate), formatter.parse(endDate));
        } else {
        	int pageNumber = Integer.parseInt(page) - 1;
            return ap3rManager.findByCreatedDateAndUserMSPageable(userIdList, formatter.parse(startDate),
                    formatter.parse(endDate), new PageRequest(pageNumber, Integer.parseInt(getCountData)));
        }
    }

    public AP3R findByLocalId(String localId) {
        return ap3rManager.findByLocalId(localId);
    }

    public AP3R findByLocalIdAndIsDeleted(String localId) {
        return ap3rManager.findByLocalIdAndIsDeleted(localId);
    }

    public List<AP3R> findAp3rListBySwId(Long swId) {
        return ap3rManager.findAp3rListBySwId(swId);
    }

    public List<AP3R> findIsDeletedAp3rList() {
        return ap3rManager.findIsDeletedAp3rList();
    }

    public void save(FundedThings product) {
        ap3rManager.save(product);
    }

    public List<FundedThings> findByAp3rId(AP3R ap3r) {
        return ap3rManager.findByAp3rId(ap3r);
    }

    public void delete(FundedThings thingsList) {
        ap3rManager.delete(thingsList);
    }

}