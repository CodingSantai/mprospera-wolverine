package id.co.telkomsigma.btpns.mprospera.dao;

import id.co.telkomsigma.btpns.mprospera.model.sda.AreaProvince;
import org.springframework.data.jpa.repository.JpaRepository;

public interface AreaProvinceDao extends JpaRepository<AreaProvince, String> {

    AreaProvince findByAreaId(String areaId);

}