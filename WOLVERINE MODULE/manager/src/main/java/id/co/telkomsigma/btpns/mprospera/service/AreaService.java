package id.co.telkomsigma.btpns.mprospera.service;

import id.co.telkomsigma.btpns.mprospera.manager.LocationManager;
import id.co.telkomsigma.btpns.mprospera.manager.AreaManager;
import id.co.telkomsigma.btpns.mprospera.model.location.Location;
import id.co.telkomsigma.btpns.mprospera.model.sda.AreaKelurahan;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service("areaService")
public class AreaService extends GenericService {

    @Autowired
    private LocationManager locationManager;

    @Autowired
    private AreaManager areaManager;

    public Location findLocationById(String id) {
        return locationManager.findByLocationId(id);
    }

    public AreaKelurahan findKelurahanById(String areaId) {
        return areaManager.findKelurahanById(areaId);
    }

}