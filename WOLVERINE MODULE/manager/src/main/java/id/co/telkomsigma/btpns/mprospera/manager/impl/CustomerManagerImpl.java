package id.co.telkomsigma.btpns.mprospera.manager.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.cache.annotation.Caching;
import org.springframework.stereotype.Service;
import id.co.telkomsigma.btpns.mprospera.dao.CustomerDao;
import id.co.telkomsigma.btpns.mprospera.manager.CustomerManager;
import id.co.telkomsigma.btpns.mprospera.model.customer.Customer;

@SuppressWarnings("RedundantIfStatement")
@Service("customerManager")
public class CustomerManagerImpl implements CustomerManager {

    @Autowired
    private CustomerDao customerDao;

    @Override
    @Caching(evict = {@CacheEvict(value = "hwk.customer.getCustomerByRrn", allEntries = true, beforeInvocation = true),
            @CacheEvict(value = "hwk.customer.countCustomerByUsername", key = "#customer.assignedUsername", beforeInvocation = true),
            @CacheEvict(value = "hwk.customer.getAllCustomerByLocPageable", beforeInvocation = true, allEntries = true),
            @CacheEvict(value = "hwk.customer.getAllCustomerByLoc", beforeInvocation = true, allEntries = true),
            @CacheEvict(value = "hwk.customer.getAllCustomerByLocAndCreatedDate", beforeInvocation = true, allEntries = true),
            @CacheEvict(value = "hwk.customer.getAllCustomerByLocAndCreatedDatePagable", beforeInvocation = true, allEntries = true),
            @CacheEvict(value = "hwk.customer.findIsDeletedCustomerList", beforeInvocation = true, allEntries = true),
            @CacheEvict(value = "hwk.customer.findByCustomerId", key = "#customer.customerId", beforeInvocation = true),
            @CacheEvict(value = "hwk.customer.isValidCustomer", key = "#customer.customerId", beforeInvocation = true),
            @CacheEvict(value = "hwk.customer.getBySwId", key = "#customer.swId", beforeInvocation = true),
            @CacheEvict(value = "hwk.customer.getNewCustomerLoanDeviation", allEntries = true, beforeInvocation = true),
            @CacheEvict(value = "hwk.customer.getExistCustomerLoanDeviation", allEntries = true, beforeInvocation = true),
            @CacheEvict(value = "hwk.customer.getNewCustomerLoanDeviationAndWisma", allEntries = true, beforeInvocation = true),
            @CacheEvict(value = "hwk.customer.getExistCustomerLoanDeviationAndWisma", allEntries = true, beforeInvocation = true),
            @CacheEvict(value = "hwk.customer.getNewCustomerLoanDeviationWithBatch", allEntries = true, beforeInvocation = true),
            @CacheEvict(value = "hwk.customer.getExistCustomerLoanDeviationWithBatch", allEntries = true, beforeInvocation = true),
            @CacheEvict(value = "hwk.customer.getNewCustomerLoanDeviationAndWismaWithBatch", allEntries = true, beforeInvocation = true),
            @CacheEvict(value = "hwk.customer.getExistCustomerLoanDeviationAndWismaWithBatch", allEntries = true, beforeInvocation = true),
            @CacheEvict(value = "wln.customer.getBySwId", key = "#customer.swId", beforeInvocation = true),
            @CacheEvict(value = "wln.customer.findById", key = "#customer.customerId", beforeInvocation = true),
            @CacheEvict(value = "wln.customer.getByPdkId", key = "#customer.pdkId", beforeInvocation = true),
            @CacheEvict(value = "irn.customer.findById", key = "#customer.customerId", beforeInvocation = true),
            @CacheEvict(value = "irn.customer.getCustomerByCifId", key = "#customer.customerCifNumber", beforeInvocation = true),
            @CacheEvict(value = "irn.customer.getBySentraId", allEntries = true, beforeInvocation = true)})
    public void save(Customer customer) {
        // TODO Auto-generated method stub
        customerDao.save(customer);
    }

    @Override
    @CacheEvict(allEntries = true, cacheNames = {"wln.customer.getBySwId", "wln.customer.findById",
            "wln.customer.getByPdkId"})
    public void clearCache() {
        // TODO Auto-generated method stub
    }

    @Override
    @Cacheable(value = "wln.customer.getBySwId", unless = "#result == null")
    public Customer getBySwId(Long swId) {
        // TODO Auto-generated method stub
        return customerDao.findTopBySwId(swId);
    }

    @Override
    @Cacheable(value = "wln.customer.findById", unless = "#result == null")
    public Customer findById(long parseLong) {
        // TODO Auto-generated method stub
        return customerDao.findOne(parseLong);
    }

    @Override
    @Cacheable(value = "wln.customer.getByPdkId", unless = "#result == null")
    public Customer getByPdkId(Long pdkId) {
        // TODO Auto-generated method stub
        return customerDao.findTopByPdkId(pdkId);
    }

}