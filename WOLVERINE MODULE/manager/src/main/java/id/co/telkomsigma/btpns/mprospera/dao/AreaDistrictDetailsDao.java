package id.co.telkomsigma.btpns.mprospera.dao;

import id.co.telkomsigma.btpns.mprospera.model.sda.AreaDistrictDetails;
import org.springframework.data.jpa.repository.JpaRepository;

public interface AreaDistrictDetailsDao extends JpaRepository<AreaDistrictDetails, Long> {

    Integer countByAreaDetailId(long parseLong);

}